//
//  CardView.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 30/12/20.
//

import SwiftUI
import Foundation
import AVFoundation
import AVKit


struct ThumbCardView: View {
    
    var image: String
    var title: String
    var heading: String
    var author: String
    var trimmed_video_url: String
    var duration: String
    var view_count: String
    let formattedDuration: String
    let formattedVisualizzations: String
    
    init(image: String, title: String, heading: String, author: String, trimmed_video_url: String, duration: String, view_count: String) {
        self.image = image
        self.title = title
        self.heading = heading
        self.author = author
        self.trimmed_video_url = trimmed_video_url
        self.duration = duration
        self.view_count = view_count
        
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.unitsStyle = .positional
        formattedDuration = formatter.string(from: TimeInterval(self.duration)!)!
        
        
        let visualizationsFormatter = NumberFormatter()
        visualizationsFormatter.numberStyle = NumberFormatter.Style.decimal
        formattedVisualizzations = visualizationsFormatter.string(for: Int(view_count))!
        
    }
    
    var body: some View {
        let url = URL(string: self.image )
        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        let thumbnail_image = UIImage(data: data!)
        

        
        VStack (alignment: .leading, spacing: 0) {
            ZStack (alignment: .bottomTrailing) {
                Image(uiImage: thumbnail_image!)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                


                Text(formattedDuration)
                    .font(.system(size: 8))
                    .padding(5)
                    .background(Color.black)
                    .cornerRadius(40)
                    .foregroundColor(.white)
                    .padding(5)
            }
        
            HStack {
                VStack(alignment: .leading) {
                    Text(self.title)
                        .font(.caption)
                        .foregroundColor(.primary).fontWeight(.regular)
                    HStack {
                        Text(self.author + "  •")
                            .font(.caption)
                            .foregroundColor(.secondary)
                        Text(formattedVisualizzations + " visualizzazioni")
                            .font(.caption)
                            .foregroundColor(.secondary)
                    }
                    
                }
                .layoutPriority(100)
         
                Spacer()
            }
            .padding(10)
            .background(Color.white)
        }
        .cornerRadius(10)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color(.sRGB, red: 150/255, green: 150/255, blue: 150/255, opacity: 0.1), lineWidth: 1)
        )
    }
}

//extension UIView {
//    func asImage() -> UIImage {
//        let format = UIGraphicsImageRendererFormat()
//        format.scale = 1
//        return UIGraphicsImageRenderer(size: self.layer.frame.size, format: format).image { context in
//            self.drawHierarchy(in: self.layer.bounds, afterScreenUpdates: true)
//        }
//    }
//}
//
//
//extension View {
//    func asImage(size: CGSize) -> UIImage {
//        let controller = UIHostingController(rootView: self)
//        controller.view.bounds = CGRect(origin: .zero, size: size)
//        let image = controller.view.asImage()
//        return image
//    }
//}

struct ThumbCardViewCardView_Previews: PreviewProvider {
    static var previews: some View {
        ThumbCardView(image: "https://i.ytimg.com/vi_webp/lnCXtHhuCko/maxresdefault.webp", title: "Quella cosa con la lingua tutorial", heading: "", author: "Nello Taver", trimmed_video_url: "https://tube2story.pythonanywhere.com/static/videos/test_flask.mp4", duration: "1249", view_count: "3490249")
    }
}
