//
//  ContentView.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 04/12/20.
//

import SwiftUI
import CoreData
import Foundation





private struct Step: Identifiable {
    var id = UUID()
    var text: String
    var image: String
    var color: Color
    var shadowColor: Color
}

struct StepView: View {

   var text = "Build an app with SwiftUI"
   var image = "Illustration1"
   var color = Color("background3")
   var shadowColor = Color("backgroundShadow3")

   var body: some View {
    HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 0){
        Text(text)
            .font(.title2)
            .fontWeight(.bold)
            .foregroundColor(.white)
            .padding(20)
            .lineLimit(5)
        
        Spacer()
        Image(image)
            .resizable()
            .renderingMode(.original)
            .aspectRatio(contentMode: .fit)
            .frame(width: 50, height: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .padding(20)
    }
    .background(color)
    .cornerRadius(30)
    .shadow(color: shadowColor, radius: 5, x: 0, y: 5)
   }
}

enum NetworkError: Error {
    case badURL, requestFailed, unknown
}

//struct ContentView: View {
//    @Environment(\.managedObjectContext) private var viewContext
//
//    @FetchRequest(
//        sortDescriptors: [NSSortDescriptor(keyPath: \Item.timestamp, ascending: true)],
//        animation: .default)
//    private var items: FetchedResults<Item>
//    
//    @State var youtube_url: String = ""
//
//    @State var result_video: Video = Video(thumbnail: "", title: "", uploader: "", trimmed_video_url: "", video_width: "", video_height: "", duration: "", view_count: "")
//    
//    @State var isNavigationBarHidden: Bool = true
//    
//    @State private var showingAlert = false
//    
//    let pasteboard = UIPasteboard.general
//    
//    @State var isActive: Bool = false
//
//    fileprivate func handleIncomingString() {
//        if let string = pasteboard.string {
//            // text was found and placed in the "string" constant
//            if string.hasPrefix("https://youtu.be/") {
//                youtube_url = string
//                self.youtube_url = string
//                self.isActive = true // this bring to the  CreationView Page
//            } else {
//                print("not a youtube link")
//            }
//        }
//    }
//    
//    var body: some View {
//        NavigationView {
//            
//            let title = "Storify"
//            
//            let steps = [
//                Step(text: "1.  Apri YouTube", image: "youtube", color: Color("Color1"), shadowColor: Color("Color1")),
//                Step(text: "2.  Trova il video che vuoi condividere", image: "search", color: Color("Color2"), shadowColor: Color("Color2")),
//                Step(text: "3.  Premi su condividi e seleziona «Copia link»", image: "share", color: Color("Color3"), shadowColor: Color("Color3")),
//                Step(text: "4.  Torna qui e attendi che il post sia pronto", image: "clock", color: Color("Color4"), shadowColor: Color("Color4"))
//            ]
//            
//            VStack(spacing:20){
//                NavigationLink(destination: StartingView2()) {
//                    Text(title).font(.largeTitle).fontWeight(.heavy)
//                }
//                
//                VStack(spacing: 20){
//                    ForEach(steps){ step in
//                        StepView(text: step.text, image: step.image, color: step.color, shadowColor: step.shadowColor)
//                            .onTapGesture {
//                                // do we need some action to be performed here?
//                            }
//                    }
//                }
//                
//                NavigationLink(destination: VincenzoCarouselView(video_url: self.youtube_url), isActive: self.$isActive) {
//                    Text("")
//                }
//                .hidden()
//                
//                Spacer()
//            }
//            .padding(10)
//            .onReceive(NotificationCenter.default.publisher(for: UIApplication.willEnterForegroundNotification)) { _ in
//                print("ContentView::onReceive->foreground")
//                handleIncomingString()
//            }
//            .onAppear(){
//                print("ContentView::onAppear")
//                handleIncomingString()
//            }
//        }
//        .navigationBarTitle("")
//        .navigationBarHidden(self.isNavigationBarHidden)
//        .onAppear {
//            self.isNavigationBarHidden = true
//        }
//    }
//}

//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView().preferredColorScheme(.light).environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
//
////        ContentView()
////            .environment(\.colorScheme, .dark)
//    }
//}
