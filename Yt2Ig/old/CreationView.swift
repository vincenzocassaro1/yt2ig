//
//  CreationView.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 08/12/20.
//

import SwiftUI
import AVFoundation

extension UIScreen{
   static let screenWidth = UIScreen.main.bounds.size.width
   static let screenHeight = UIScreen.main.bounds.size.height
   static let screenSize = UIScreen.main.bounds.size
}

extension UIView {
    func asImage(rect: CGRect) -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: rect)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}

struct CreationView: View {
    
//    let youtube_url: String
    
    @State private var rect1: CGRect = .zero
    @State private var uiimage: UIImage? = nil
    
    var result_video: Video
    
    private let editor = VideoEditor()
    
    private let urlScheme = URL(string: "instagram-stories://share")!
    
    enum optionsKey: String {
        case StickerImage = "com.instagram.sharedSticker.stickerImage"
        case bgImage = "com.instagram.sharedSticker.backgroundImage"
        case bgVideo = "com.instagram.sharedSticker.backgroundVideo"
        case bgTopColor = "com.instagram.sharedSticker.backgroundTopColor"
        case bgBottomColor = "com.instagram.sharedSticker.backgroundBottomColor"
        case contentUrl = "com.instagram.sharedSticker.contentURL"
    }
    
    var body: some View {
    
        
            VStack(alignment: .center, spacing: 50) {
                
                VStack{
                    VStack {
                        Spacer()
                        
                        ThumbCardView(image: result_video.thumbnail, title: result_video.title, heading: "", author: result_video.uploader, trimmed_video_url: result_video.trimmed_video_url, duration: result_video.duration, view_count: result_video.view_count)
                        
    //                    VStack(alignment: .leading){
    //                        let url = URL(string: result_video.thumbnail )
    //                        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
    //                        let thumbnail_image = UIImage(data: data!)
    //
    //                        //                Spacer().frame(height: 30)
    //
    //                        //                    HStack{
    //                        //                        Image("youtube")
    //                        //                            .resizable()
    //                        //                            .frame(width: 48, height: 48)
    //                        //
    //                        //                        Text("Tube2Story")
    //                        //                    }
    //                        //                    .padding(15)
    //                        //                    .padding(.horizontal, 0)
    //
    //                        VStack(alignment: .leading, spacing: 0.0) {
    //
    //                            ZStack (alignment: .bottomLeading) {
    //                                Image(uiImage: thumbnail_image!)
    //                                    .resizable()
    //                                    .padding(.horizontal, -11.0)
    //                                    .frame(width: UIScreen.screenWidth*0.7, height: (UIScreen.screenWidth*0.7)*9/16)
    //                                //                                .cornerRadius(6)
    //                                //                                .shadow(radius: 10)
    //
    //                                HStack(){
    //                                    //                            Image("youtube")
    //                                    //                                .resizable()
    //                                    //                                .frame(width: 24, height: 24)
    //                                    //                                .clipShape(Circle())
    //
    //                                    //                                Text(result_video.uploader)
    //                                    //                                    .font(.system(size: 10))
    //                                    //                                    .padding(5)
    //                                    //                                    .background(Color.black)
    //                                    //                                    .cornerRadius(40)
    //                                    //                                    .foregroundColor(.white)
    //                                }.padding(5)
    //                            }
    //                            .padding(.horizontal, 0.0)
    //
    //
    //                            Text(result_video.title)
    //                                .lineLimit(1)
    //                                .minimumScaleFactor(0.2)
    //                                .font(.system(size: 14))
    //                                .padding(.top, 10.0)
    //
    //
    //                            Text(result_video.uploader)
    //                                .font(.system(size: 10))
    //                                .padding(.vertical, 5.0)
    //                        }
    //                        .padding(0)
    //
    //                        //                Spacer()
    //                    }
    //                    .frame(width: UIScreen.screenWidth * 0.75)
    //                    .background(Color.white)
    //                    .cornerRadius(10)
    //                    .shadow(radius: 10)
                        
                        Spacer()
                    }
                    //            .cornerRadius(10)
                    .frame(width: 1080*0.3, height: 1920*0.3, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    
    //                .frame(minWidth: /*@END_MENU_TOKEN@*/, maxWidth: 1080, minHeight: /*@START_MENU_TOKEN@*/0, maxHeight: 1920, alignment: .center)
                    //            .padding()
                    .background(Color.white)
                    
//                    .background(RectGetter(rect: $rect1))
                    .cornerRadius(10)
                    .shadow(radius: 10)
                }
                .padding()
//                .background(Color.white)
//                .cornerRadius(40)
                
                
                Button(action: {
                    self.uiimage = UIApplication.shared.windows[0].rootViewController?.view.asImage(rect: self.rect1)
                    
                    // here -just before share to instagram- we should create the video
//                    let result_url = URL(string: result_video.trimmed_video_url)
//                    loadFileAsync(url: result_url!) { (path, error) in
//                        print("video File downloaded to : \(path!)")
//                        let __url = URL(string: "file:///private"+path!)
//                        editor.renderVideo(fromVideoAt: __url!, forName: "") { exportedURL in
//                            print(exportedURL)
//                            post(bgVideoUrl: exportedURL!)
////                            shareToInstagramStories()
//                        }
//                    }
                    shareToInstagramStories()

                }) {
                    Text("Share to Instagram")
                }
                .frame(minWidth: 0, maxWidth: 300)
                .font(.system(size: 18))
                .padding()
                .background(LinearGradient(gradient: Gradient(colors: [Color("Color1"), Color("Color2"), Color("Color3"), Color("Color4"), Color("Color5")]), startPoint: .leading, endPoint: .trailing))
                .cornerRadius(40)
                .foregroundColor(.white)
                
                //            Spacer()
            }
            .frame(minWidth: 0/*@END_MENU_TOKEN@*/, maxWidth: .infinity, minHeight: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, maxHeight: /*@START_MENU_TOKEN@*/.infinity, alignment: .center)
//            .background(Color.white)
//            .padding(0)
//            .cornerRadius(40)
            .navigationBarTitle("SwiftUI", displayMode: .inline)


    }
    
    func post(bgVideoUrl:URL, stickerImage:UIImage? = nil, contentURL:String? = nil) -> Bool{
        var items:[[String : Any]] = [[:]]
        //Background Video
        var videoData:Data?
        do {
            try videoData = Data(contentsOf: bgVideoUrl)
        } catch {
            print("Cannot open \(bgVideoUrl)")
            return false
        }
        items[0].updateValue(videoData as Any, forKey: optionsKey.bgVideo.rawValue)
        //Sticker Image
        if stickerImage != nil {
            let stickerData = stickerImage!.pngData()!
            items[0].updateValue(stickerData, forKey: optionsKey.StickerImage.rawValue)
        }
        //Content URL
        if contentURL != nil {
            items[0].updateValue(contentURL as Any, forKey: optionsKey.contentUrl.rawValue)
        }
        let isPosted = post(items)
        return isPosted
    }
    
    private func post(_ items:[[String : Any]]) -> Bool{
        guard UIApplication.shared.canOpenURL(urlScheme) else {
            print("Cannot open \(urlScheme)")
            return false
        }
        let options: [UIPasteboard.OptionsKey: Any] = [.expirationDate: Date().addingTimeInterval(60 * 5)]
        UIPasteboard.general.setItems(items, options: options)
        UIApplication.shared.open(urlScheme)
        return true
    }
    

    func shareToInstagramStories() {
        guard let instagramStoryUrl = URL(string: "instagram-stories://share") else { return }
        guard UIApplication.shared.canOpenURL(instagramStoryUrl) else { return }
        
//        ###################### POST PNG
        
        let imagePNGData = self.uiimage!
        

//            let imageData: Data = UIImage(imageLiteralResourceName: "background_test").pngData()!
        

//            "com.instagram.sharedSticker.stickerImage": imageData,
//            "com.instagram.sharedSticker.backgroundImage": imagePNGData
        let itemsToShare: [[String: Any]] = [[ "com.instagram.sharedSticker.backgroundImage": imagePNGData]]
        
        
//        ######################
        
        let pasteboardOptions: [UIPasteboard.OptionsKey: Any] = [.expirationDate: Date().addingTimeInterval(60 * 5)]
        UIPasteboard.general.setItems(itemsToShare, options: pasteboardOptions)
        UIApplication.shared.open(instagramStoryUrl, options: [:], completionHandler: nil)
    }
}

//struct RectGetter: View {
//    @Binding var rect: CGRect
//
//    var body: some View {
//        GeometryReader { proxy in
//            self.createView(proxy: proxy)
//        }
//    }
//
//    func createView(proxy: GeometryProxy) -> some View {
//        DispatchQueue.main.async {
//            self.rect = proxy.frame(in: .global)
//        }
//
//        return Rectangle().fill(Color.clear)
//    }
//}

func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
  return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
}

//struct CreationView_Previews: PreviewProvider {
//    static var previews: some View {
////        NavigationView {
//            CreationView(result_video: Video(thumbnail: "https://i.ytimg.com/vi_webp/lnCXtHhuCko/maxresdefault.webp", title: "QUELLA COSA CON LA LINGUA TUTORIAL", uploader: "NELLO TAVER", trimmed_video_url: "https://tube2story.pythonanywhere.com/static/videos/test_flask.mp4", duration: "1249", view_count: "3490249"))
////        }
////        .navigationBarTitle("SwiftUI", displayMode: .inline)
//
//    }
//}
