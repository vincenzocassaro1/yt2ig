//
//  OldEditorView.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 14/12/20.
//

import SwiftUI

//extension UIScreen{
//   static let screenWidth = UIScreen.main.bounds.size.width
//   static let screenHeight = UIScreen.main.bounds.size.height
//   static let screenSize = UIScreen.main.bounds.size
//}
//
//extension UIView {
//    func asImage(rect: CGRect) -> UIImage {
//        let renderer = UIGraphicsImageRenderer(bounds: rect)
//        return renderer.image { rendererContext in
//            layer.render(in: rendererContext.cgContext)
//        }
//    }
//}

struct OldEditorView: View {
    //    let youtube_url: String
        
        @State private var rect1: CGRect = .zero
        @State private var uiimage: UIImage? = nil
        
        var result_video: Video
        
        var body: some View {
            VStack(alignment: .center, spacing: 50) {
            
                
                VStack(alignment: .leading){
                    let url = URL(string: result_video.thumbnail )
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    let thumbnail_image = UIImage(data: data!)
                    
                    Spacer().frame(height: 30)
                    
                    HStack{
                        Image("youtube")
                            .resizable()
                            .frame(width: 48, height: 48)
                        
                        Text("Tube2Story")
                    }
                    .padding(15)
                    
                    VStack(spacing: 0) {
                        
                        ZStack (alignment: .bottomLeading) {
                            Image(uiImage: thumbnail_image!)
                                .resizable()
                                .frame(width: UIScreen.screenWidth-80, height: (UIScreen.screenWidth-80)*9/16)
                                .cornerRadius(10)
                                .shadow(radius: 10)
                            
                            HStack(){
    //                            Image("youtube")
    //                                .resizable()
    //                                .frame(width: 24, height: 24)
    //                                .clipShape(Circle())
                                
                                Text(result_video.uploader)
                                    .font(.system(size: 10))
                                    .padding(5)
                                    .background(Color.black)
                                    .cornerRadius(40)
                                    .foregroundColor(.white)
                            }.padding(5)
                        }
            
                        Text(result_video.title)
                            .font(.body)
                            .frame(width: UIScreen.screenWidth-80, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .topLeading)
                            .padding()
                    }
        
                    Spacer()
                }
                .frame(width: UIScreen.screenWidth-50, height: (UIScreen.screenWidth-50)*16/9)
                .background(Color.white)
                .cornerRadius(10)
                .shadow(radius: 10)
//                .background(RectGetter(rect: $rect1))
                
                Button(action: {
                    self.uiimage = UIApplication.shared.windows[0].rootViewController?.view.asImage(rect: self.rect1)
                    shareToInstagramStories()
                }) {
                    Text("Share to Instagram")
                }
                .frame(minWidth: 0, maxWidth: 300)
                .font(.system(size: 18))
                .padding()
                .background(LinearGradient(gradient: Gradient(colors: [Color("Color1"), Color("Color2"), Color("Color3"), Color("Color4"), Color("Color5")]), startPoint: .leading, endPoint: .trailing))
                .cornerRadius(40)
                .foregroundColor(.white)

                Spacer()
            }

            .frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, minHeight: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, maxHeight: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: .center)
            .background(Color.white)
            .padding(0)
 
        }
        
 
    //
        func shareToInstagramStories() {
    //        guard let imagePNGData = getImageWithColor(color: UIColor.red, size: CGSize(width: 1000, height: 1820)).pngData() else { return }
            
            let imagePNGData = self.uiimage!
                guard let instagramStoryUrl = URL(string: "instagram-stories://share") else { return }
                guard UIApplication.shared.canOpenURL(instagramStoryUrl) else { return }
            // 3
    //            let imageData: Data = UIImage(imageLiteralResourceName: "background_test").pngData()!
            
            // 4
    //            "com.instagram.sharedSticker.stickerImage": imageData,
                let itemsToShare: [[String: Any]] = [[ "com.instagram.sharedSticker.backgroundImage": imagePNGData]]
                let pasteboardOptions: [UIPasteboard.OptionsKey: Any] = [.expirationDate: Date().addingTimeInterval(60 * 5)]
                UIPasteboard.general.setItems(itemsToShare, options: pasteboardOptions)
                UIApplication.shared.open(instagramStoryUrl, options: [:], completionHandler: nil)
            }

}

//struct OldEditorView_Previews: PreviewProvider {
//    static var previews: some View {
//        OldEditorView(result_video: Video(thumbnail: "https://i.ytimg.com/vi_webp/lnCXtHhuCko/maxresdefault.webp", title: "QUELLA COSA CON LA LINGUA TUTORIAL", uploader: "NELLO TAVER", trimmed_video_url: "https://tube2story.pythonanywhere.com/static/videos/test_flask.mp4", duration: "1249", view_count: "3490249"))
//    }
//}
