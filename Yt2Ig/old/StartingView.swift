//
//  StartingView.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 12/09/21.
//

import SwiftUI

struct VisualEffectView: UIViewRepresentable {
    var effect: UIVisualEffect?
    func makeUIView(context: UIViewRepresentableContext<Self>) -> UIVisualEffectView { UIVisualEffectView() }
    func updateUIView(_ uiView: UIVisualEffectView, context: UIViewRepresentableContext<Self>) { uiView.effect = effect }
}

struct StartingView: View {
    @State var url: String = ""
    
    var body: some View {
        Glassmorphism()
//        VStack(alignment: .center){
//            ZStack {
//
//                Image("maxresdefault")
//                    .resizable()
//                    .scaledToFill()
////                    .edgesIgnoringSafeArea(.all)
//                    .blur(radius: 20)
//
////                VisualEffectView(effect: UIBlurEffect(style: .dark))
////                    .edgesIgnoringSafeArea(.all)
//
//                VStack {
//                    Text("Storify").font(.largeTitle).fontWeight(.heavy)
//
//                    HStack {
//                        TextField("Past the Youtube url here", text: $url)
//                            .textContentType(.URL)
//                            .textFieldStyle(RoundedBorderTextFieldStyle())
//                            .multilineTextAlignment(.center)
//
//                        Button(action: {
//            //                openURL(URL(string: url)!)
//                        }) {
//                            Image(systemName: "arrow.right.circle.fill")
//                        }
//                        .disabled(url == "")
//                    }
//                }
//                .padding()
//            }
//        }
//        .padding()
    }
}

struct Glassmorphism: View {
    
    @State private var animateshape = false
    
    var body: some View {
        ZStack {
            LinearGradient(gradient: Gradient(colors: [.purple, .pink.opacity(0.3), .pink]), startPoint: .topLeading, endPoint: .bottomTrailing).edgesIgnoringSafeArea(.all)
           
           
            
            ZStack {
                
                //Circle
               Image("e1")
                .resizable()
                    .frame(width: 300, height: 300)
                    .offset(x: 150, y: -200)
                    .foregroundColor(Color(#colorLiteral(red: 1, green: 0.6699851155, blue: 0.8290407062, alpha: 1)).opacity(1))
                    .blur(radius: 10)
                    .scaleEffect(animateshape ? 0.8 : 1)
                    .animation(Animation.easeIn(duration: 10).repeatForever(autoreverses: true))
               
                    
                     
                //pyramid
                Image("e2")
                 .resizable()
                     .frame(width: 300, height: 300)
                     .offset(x: -200, y: 350)
                     .foregroundColor(Color(#colorLiteral(red: 1, green: 0.6699851155, blue: 0.8290407062, alpha: 1)).opacity(1))
                     .blur(radius: 10)
                     .scaleEffect(animateshape ? 0.8 : 1)
                    .animation(Animation.easeIn(duration: 10).repeatForever(autoreverses: true))
                   
                GlassWindow()
//                    .offset(x: animateshape ? 0 : 400, y: 0)
                   
//
//                GlassWindow()
//                    .offset(x: animateshape ? -400 : 0, y: 0)
                   
                
                //semi-circle
                Image("e3")
                 .resizable()
                     .frame(width: 300, height: 300)
                     .foregroundColor(Color(#colorLiteral(red: 1, green: 0.6699851155, blue: 0.8290407062, alpha: 1)).opacity(1))
                      .blur(radius: 1)
                    .rotationEffect(.degrees(animateshape ? 0 : 360))
                    .offset(x: 150, y: 200)
                    .animation(Animation.easeIn(duration: 30).repeatForever(autoreverses: true))
                    
                
                
                //square
                Image("e4")
                 .resizable()
                     .frame(width: 200, height: 200)
                     .foregroundColor(Color(#colorLiteral(red: 1, green: 0.6699851155, blue: 0.8290407062, alpha: 1)).opacity(1))
                     .blur(radius: 1)
                    .rotationEffect(.degrees(animateshape ? 30 : -30))
                    .offset(x: -150, y: -300)
                    .animation(Animation.easeIn(duration: 10).repeatForever(autoreverses: true))

                   
                
            }
//             .animation(Animation.easeIn(duration: 2).repeatForever(autoreverses: true))
             .onAppear(){
                    self.animateshape.toggle()
                        }
            
        }
        
    }
}
//

struct GlassWindow: View {
    @State var url: String = ""
    
    var body: some View {
        
        ZStack() {
            RoundedRectangle(cornerRadius: 25.0)
                .foregroundColor(.white).opacity(0.3)
                .frame(width: 380, height: 480)
                .shadow(color: Color.black.opacity(0.05), radius: 10, x: 0, y: 10)
                .blur(radius: 1)
            
                VStack {
                    Text("Storify").font(.largeTitle).fontWeight(.heavy)

                    HStack {
                        TextField("Past the Youtube url here", text: $url)
                            .textContentType(.URL)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .multilineTextAlignment(.center)

                        Button(action: {
            //                openURL(URL(string: url)!)
                        }) {
                            Image(systemName: "arrow.right.circle.fill")
                        }
                        .disabled(url == "")
                    }
                }
                .padding()
        }
        .padding()
    }
}


struct StartingView_Previews: PreviewProvider {
    static var previews: some View {
        StartingView()
    }
}
