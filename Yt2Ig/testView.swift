//
//  testView.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 11/12/20.
//

import SwiftUI

struct testView: View {
    var body: some View {
        TabView{
            NavigationView{
                VStack{
                    Text("DDDDDDD")
                }
                
            }
            .tabItem{
                Button(action: {
    //                self.uiimage = UIApplication.shared.windows[0].rootViewController?.view.asImage(rect: self.rect1)
    //                shareToInstagramStories()
                }) {
                    Text("Share to Instagram")
                }
                .frame(minWidth: 0, maxWidth: 300)
                .font(.system(size: 18))
                .padding()
                .background(LinearGradient(gradient: Gradient(colors: [Color("Color1"), Color("Color2"), Color("Color3"), Color("Color4"), Color("Color5")]), startPoint: .leading, endPoint: .trailing))
                .cornerRadius(40)
                .foregroundColor(.white)
            }
        }

    }
}

struct testView_Previews: PreviewProvider {
    static var previews: some View {
        testView()
    }
}
