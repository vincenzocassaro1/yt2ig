//
//  BottomCardView.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 24/08/21.
//

import SwiftUI

struct BottomCardView: View {
    
    var title: String
    var author: String
    var view_count: String
    var video_width: String
    var video_height: String
    var allCornersRounded: Bool
    
    var body: some View {
        let title = title
        let author = author
        let view_count = view_count
        let video_ratio = Int(video_width)! / Int(video_height)!
        let desired_card_width = CGFloat(1080 * 0.30 - 40)
        
        HStack {
            VStack(alignment: .leading) {
                Text(title)
                    .font(.caption)
                    .foregroundColor(.primary)
                    .fontWeight(.regular)
                HStack {
                    Text(author + "  • " + view_count + " visualizzazioni")
                        .font(.caption)
                        .foregroundColor(.secondary)
                }
                
            }
            .layoutPriority(100)
            Spacer()


     
        }
        .padding(10)
        .background(Color.white)
        .cornerRadius(10, corners: [.bottomLeft, .bottomRight])
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color(.sRGB, red: 150/255, green: 150/255, blue: 150/255, opacity: 0.1), lineWidth: 1)
        )

    }
    
    var corners: [UIRectCorner] {
        return (self.allCornersRounded) ? [.bottomLeft, .bottomRight, .topLeft, .topRight] : [.bottomLeft, .bottomRight]
    }
}

extension View {
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape( RoundedCorner(radius: radius, corners: corners) )
    }
}

struct RoundedCorner: Shape {

    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners

    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}

// Extension
extension View {
    var asImage: UIImage {
        // Must ignore safe area due to a bug in iOS 15+ https://stackoverflow.com/a/69819567/1011161
        let controller = UIHostingController(rootView: self.edgesIgnoringSafeArea(.top))
        let view = controller.view
        let targetSize = controller.view.intrinsicContentSize
        view?.bounds = CGRect(origin: CGPoint(x: 0, y: 0), size: targetSize)
        view?.backgroundColor = .clear

        let format = UIGraphicsImageRendererFormat()
        format.scale = 3 // Ensures 3x-scale images. You can customise this however you like.
        let renderer = UIGraphicsImageRenderer(size: targetSize, format: format)
        return renderer.image { _ in
            view?.drawHierarchy(in: controller.view.bounds, afterScreenUpdates: true)
        }
    }
}

//extension View {
//    func snapshot() -> UIImage {
//        let controller = UIHostingController(rootView: self)
//        let view = controller.view
//
//        let targetSize = controller.view.intrinsicContentSize
//        view?.bounds = CGRect(origin: .zero, size: targetSize)
//        view?.backgroundColor = .clear
//
//        let renderer = UIGraphicsImageRenderer(size: targetSize)
//
//        return renderer.image { _ in
//            view?.drawHierarchy(in: controller.view.bounds, afterScreenUpdates: true)
//        }
//    }
//}
//
//extension View {
//    func asImage() -> UIImage {
//        let controller = UIHostingController(rootView: self)
//
//        // locate far out of screen
//        controller.view.frame = CGRect(x: 0, y: CGFloat(Int.max), width: 1, height: 1)
//        UIApplication.shared.windows.first!.rootViewController?.view.addSubview(controller.view)
//
//        let size = controller.view.intrinsicContentSize //controller.sizeThatFits(in: UIScreen.main.bounds.size)
//        controller.view.bounds = CGRect(origin: .zero, size: size)
//        controller.view.backgroundColor = UIColor.systemBackground
//        controller.view.sizeToFit()
//
//        //let image = controller.view.asImage()
//        let image = UIImage(view: controller.view)
//        controller.view.removeFromSuperview()
//        return image
//    }
//}
//
//extension UIImage {
//    convenience init(view: UIView) {
//        UIGraphicsBeginImageContextWithOptions((view.frame.size), false, 0.0)
//        view.layer.render(in:UIGraphicsGetCurrentContext()!)
//        let image = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        self.init(cgImage: image!.cgImage!)
//    }
//}


struct BottomCardView_Previews: PreviewProvider {
    static var previews: some View {
        BottomCardView(
            title: "Quella cosa con la lingua tutorial",
            author: "Nello Taver",
            view_count: "3490249",
            video_width: "1920",
            video_height: "1080",
            allCornersRounded: false
        )
        .frame(width: 1080*0.30 - 40 , alignment: .center)
    }
}
