//
//  CardView.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 30/12/20.
//

import SwiftUI
import Foundation
import AVFoundation
import AVKit

//TODO: think about put player classes this in a separate file

// useful link
//https://benoitpasquier.com/playing-video-avplayer-swiftui/

enum PlayerGravity {
    case aspectFill
    case resize
}

class PlayerView: UIView {
    
    var player: AVPlayer? {
        get {
            return playerLayer.player
        }
        set {
            playerLayer.player = newValue
        }
    }
    
    let gravity: PlayerGravity
    
    init(player: AVPlayer, gravity: PlayerGravity) {
        self.gravity = gravity
        super.init(frame: .zero)
        self.player = player
        self.backgroundColor = .black
        setupLayer()
    }
    
    func setupLayer() {
        switch gravity {
        
        case .aspectFill:
            playerLayer.contentsGravity = .resizeAspectFill
            playerLayer.videoGravity = .resizeAspectFill
            
        case .resize:
            playerLayer.contentsGravity = .resize
            playerLayer.videoGravity = .resize
            
        }
    }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var playerLayer: AVPlayerLayer {
        return layer as! AVPlayerLayer
    }
    
    // Override UIView property
    override static var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
}

final class PlayerContainerView: UIViewRepresentable {
    typealias UIViewType = PlayerView
    
    let player: AVPlayer
    let gravity: PlayerGravity
    
    init(player: AVPlayer, gravity: PlayerGravity) {
        self.player = player
        self.gravity = gravity
    }
    
    func makeUIView(context: Context) -> PlayerView {
        return PlayerView(player: player, gravity: gravity)
    }
    
    func updateUIView(_ uiView: PlayerView, context: Context) { }
}

class PlayerViewModel: ObservableObject {

    let player: AVPlayer
    
    init(video_url: String) {
        self.player = AVPlayer(url: URL(string: video_url)!)
        print("init PlayerViewModel")
    }
    
    @Published var isPlaying: Bool = true {
        didSet {
            if isPlaying {
                play()
            } else {
                pause()
            }
        }
    }
    
    func play() {
        let currentItem = player.currentItem
        if currentItem?.currentTime() == currentItem?.duration {
            currentItem?.seek(to: .zero, completionHandler: nil)
        }
        
        player.play()
    }
    
    func pause() {
        player.pause()
    }
}

enum PlayerAction {
    case play
    case pause
}


struct VideoCardView: View {
    
    var video: Video
    
    @State var video_is_ready: Bool = false
    
    @ObservedObject var model: PlayerViewModel
    
    private var fd = ""
    init(
        video: Video
    ) {
        self.video = video
        
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.unitsStyle = .positional
        self.fd = formatter.string(from: TimeInterval(self.video.duration)!)!
        
        model = PlayerViewModel(video_url: self.video.trimmed_video_url )
    }
    
    var body: some View {
        let url = URL(string: self.video.thumbnail )
        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        
        let desired_card_width = CGFloat(1080 * 0.3 - 40)
        let calculated_heigth_based_on_ratio = desired_card_width * CGFloat(Int(self.video.video_height)!) / CGFloat(Int(self.video.video_width)!)
        
        VStack (alignment: .leading, spacing: 0) {
            ZStack (alignment: .bottomTrailing) {
                ZStack {
                    PlayerContainerView(player: model.player, gravity: .resize)
                        .frame(
                            width: desired_card_width,
                            height: calculated_heigth_based_on_ratio
                        ).onAppear() {
                            model.play()
                        }
                }

                Text(self.fd)
                    .font(.system(size: 8))
                    .padding(5)
                    .background(Color.black)
                    .cornerRadius(40)
                    .foregroundColor(.white)
                    .padding(5)
            }

            BottomCardView(title: self.video.title, author: self.video.uploader, view_count: self.video.view_count, video_width: self.video.video_width, video_height: self.video.video_height, allCornersRounded: false)
                .frame(width: desired_card_width, alignment: .center)
            
        }
        .background(Color.white)
        .cornerRadius(10)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color(.sRGB, red: 150/255, green: 150/255, blue: 150/255, opacity: 0.1), lineWidth: 1)
        )
    }
}

//struct VideoCardView_Previews: PreviewProvider {
//    
//    static let video = Video(
//        thumbnail: "https://i.ytimg.com/vi_webp/lnCXtHhuCko/maxresdefault.webp",
//        title: "Quella cosa con la lingua tutorial",
//        uploader: "Nello Taver Nello Taver",
//        trimmed_video_url: "https://tube2story.pythonanywhere.com/static/videos/test_flask.mp4",
//        video_width: "1920",
//        video_height: "1080",
//        duration: "1249",
//        view_count: "3490249",
//        channel_assets: [""]
//    )
//    
//    static var previews: some View {
//        VideoCardView(video: video)
//    }
//}
