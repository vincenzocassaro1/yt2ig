//
//  ThumbnailView.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 08/01/23.
//

import SwiftUI
import AVFoundation

struct ThumbnailView: View {
    let url: URL

    var body: some View {
        ZStack {
            Image(uiImage: getThumbnail())
                .resizable()
                .aspectRatio(contentMode: .fit)
            Text(url.lastPathComponent)
                .font(.caption)
        }
    }

    func getThumbnail() -> UIImage {
        let asset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        do {
            let thumbnailCGImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60), actualTime: nil)
            return UIImage(cgImage: thumbnailCGImage)
        } catch {
            print("Error generating thumbnail: \(error)")
            return UIImage()
        }
    }
}

//struct ThumbnailView_Previews: PreviewProvider {
//    static var previews: some View {
//        ThumbnailView()
//    }
//}
