//
//  AnimatedLoaderView.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 09/03/22.
//

import SwiftUI

struct AnimatedLoaderView: View {
    
    var body: some View{
        ZStack{
            
            Color.white
                .frame(width: 150, height: 150)
                .cornerRadius(14)
                //Shadows
                .shadow(color: Color.primary.opacity(0.07), radius: 5, x: 5, y: 5)
                .shadow(color: Color.primary.opacity(0.07), radius: 5, x: -5, y: -5)
            
            ProgressView()
                .progressViewStyle(CircularProgressViewStyle(tint: .blue))
        }
    }
}

struct AnimatedLoaderView_Previews: PreviewProvider {
    static var previews: some View {
        AnimatedLoaderView()
    }
}
