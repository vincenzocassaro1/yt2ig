//
//  RatingCardView.swift
//  design_to_code29
//
//  Created by Dheeraj Kumar Sharma on 11/04/21.
//

import SwiftUI

struct RatingCardView: View {
    
    // MARK:- PROPERTIES
    
    @State var selectedEmoji: String = ""
    @State var hideHeader: Double = 1
    @State var sendOffset: CGFloat = 0
    @State var sendWidth: CGFloat = 150
    @State var removeText: Bool = false
    
    @Binding var scaleEffect1: CGFloat
    @Binding var scaleEffect2: CGFloat
    @Binding var url: String
    
    let pasteboard = UIPasteboard.general
    @State private var showingAlert = false
    @State private var url_ok = true
    
    
    fileprivate func handleIncomingString() {
        
        print("suca")
        
        hideHeader = 1
        sendOffset = 0
        sendWidth = 150
        removeText = false
        
        if !pasteboard.hasStrings { return }
        
        UIPasteboard.general.detectPatterns(for: [UIPasteboard.DetectionPattern.probableWebURL], completionHandler: {result in
            switch result {
                case .success(let detectedPatterns):
                    // A pattern detection is completed,
                    // regardless of whether the pasteboard has patterns we care about.
                    // So we have to check if the detected patterns contains our patterns.
                    
                    if detectedPatterns.contains(UIPasteboard.DetectionPattern.probableWebURL) {
                        // Will match if the pasteboard string has a URL within it
                        let _u = UIPasteboard.general.string!
                        if _u.hasPrefix("https://youtu.be/") {
                            // text was found and placed in the "string" constant
                            self.url_ok = true
                            self.url = _u
                        } else {
                            print("not a youtube link")
                            showingAlert = true
                        }
                    }
                    else {
                        // We won't be retrieving the value, so we won't get a notification banner
                        print("Not something we want to deal with")
//                        showingAlert = true
                    }
                case .failure(let error):
                    // This never gets called
                    print(error.localizedDescription)
                }
        })
    }
    
    fileprivate func checkString() -> Bool {
        
        print("milla")
        
        hideHeader = 1
        sendOffset = 0
        sendWidth = 150
        removeText = false
        
        
        if let myString = UIPasteboard.general.string {
            if myString.hasPrefix("https://youtu.be/")
//                || myString.hasPrefix("https://www.youtube.com/watch?v=")
//                || myString.hasPrefix("https://youtube.com/watch?v=")
            {
                // text was found and placed in the "string" constant
                self.url_ok = true
                self.url = myString
                return true;
            } else {
                print("not a youtube link")
                print(myString)
                showingAlert = true
                return false;
            }
        }
        return false
    }
    
    func cleanCard(){
        hideHeader = 1
        sendOffset = 0
        sendWidth = 150
        removeText = false
    }
    
    // MARK:- BODY
    var body: some View {
        VStack {
            // Header Content
            VStack {
                Text("Paste a YouTube video Url")
                    .font(Font.custom("Avenir-Heavy", size: 19))
                    .foregroundColor(Color.init(red: 13/255, green: 14/255, blue: 64/255))
                    .multilineTextAlignment(.center)
                Text("and see the magic happen")
                    .font(Font.custom("Avenir-Medium", size: 16))
                    .foregroundColor(Color.init(red: 159/255, green: 163/255, blue: 164/255))
                    .padding(.top,5)
                
                TextField("", text: $url)
                    .placeholder(when: url.isEmpty){
                        Text("Paste the Youtube url here")
                            .foregroundColor(.gray)
                            .textContentType(.URL)
                            .multilineTextAlignment(.center)
                    }
                    .foregroundColor(Color.init(red: 13/255, green: 14/255, blue: 64/255))
                    .padding(5)
                    .overlay(
                            RoundedRectangle(cornerRadius: 20)
                                .stroke(Color.init(red: 159/255, green: 163/255, blue: 164/255), lineWidth: 0.5)
                        )
                    .padding(.top, 20)
            }
            .opacity(hideHeader)
            .alert(isPresented: $showingAlert) {
                Alert(title: Text("Not a valid url"), message: Text("the url must follow the form 'https://youtu.be/<video_id>' without the 'watch?v=' part"), dismissButton: .cancel())
            }
            
            Button {
                if !checkString(){
                    return;
                }
                
                withAnimation(.spring()){
                    hideHeader = 0
                    sendOffset = -116
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        withAnimation(Animation.easeInOut(duration: 0.3)){
                            sendWidth = 100
                            removeText = true
                            print("primo step")
                        }
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        withAnimation(Animation.easeInOut(duration: 0.3)){
                            sendWidth = 45
                            print("secondo step")

                        }
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        withAnimation(Animation.easeInOut(duration: 1)){
                            scaleEffect1 = 50
                            print("terzo step")

                        }
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.8) {
                        withAnimation(Animation.easeInOut(duration: 1)){
                            scaleEffect2 = 50
                            print("quarto step")
                        }
                    }
                }
            } label: {
                ZStack {
                    Capsule()
                        .fill(Color.init(red: 236/255, green: 61/255, blue: 107/255))
                        .frame(width: sendWidth, height: 45)
                    if removeText {
                        if sendWidth == 45 {
                            Image("check")
                                .resizable()
                                .frame(width: 20, height: 20)
                        } else {
                            HStack {
                                Image("check")
                                    .resizable()
                                    .frame(width: 20, height: 20)
                                Text("Sent")
                                    .font(Font.custom("Avenir-Heavy", size: 15))
                                    .foregroundColor(Color.white)
                            }
                        }
                    } else {
                        Text("Send")
                            .font(Font.custom("Avenir-Heavy", size: 15))
                            .foregroundColor(Color.white)
                    }
                    
                } //: ZSTACK
                .opacity(!url_ok ? 0.7 : 1)
            } //: BUTTON
            .offset(x: 0.0, y: sendOffset)
            .disabled(!url_ok)
            .padding()

        }//: VSTACK
        .padding(20)
        .background(Color.white)
        .cornerRadius(20)
        .onReceive(NotificationCenter.default.publisher(for: UIApplication.willEnterForegroundNotification)) { _ in
//            handleIncomingString()
            cleanCard()
        }
        .onAppear(){
//            handleIncomingString()
            cleanCard()
        }
    }
}

extension View {
    func placeholder<Content: View>(
        when shouldShow: Bool,
        alignment: Alignment = .leading,
        @ViewBuilder placeholder: () -> Content) -> some View {

        ZStack(alignment: alignment) {
            placeholder().opacity(shouldShow ? 1 : 0)
            self
        }
    }
}

struct RatingCardView_Previews: PreviewProvider {
    @State var scaleEffect1: CGFloat = 0.01
    @State var scaleEffect2: CGFloat = 0.01
    @State var url: String = ""

    static var previews: some View {

        RatingCardView(scaleEffect1: .constant(0) , scaleEffect2: .constant(0), url: .constant(""))
            .preferredColorScheme(.dark)
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
