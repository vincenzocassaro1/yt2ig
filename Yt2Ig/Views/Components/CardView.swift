//
//  CardView.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 30/12/20.
//

import SwiftUI
import Foundation
import AVFoundation
import AVKit

//TODO: think about put player classes this in a separate file

// useful link
//https://benoitpasquier.com/playing-video-avplayer-swiftui/

//enum PlayerGravity {
//    case aspectFill
//    case resize
//}
//
//class PlayerView: UIView {
//
//    var player: AVPlayer? {
//        get {
//            return playerLayer.player
//        }
//        set {
//            playerLayer.player = newValue
//        }
//    }
//
//    let gravity: PlayerGravity
//
//    init(player: AVPlayer, gravity: PlayerGravity) {
//        self.gravity = gravity
//        super.init(frame: .zero)
//        self.player = player
//        self.backgroundColor = .black
//        setupLayer()
//    }
//
//    func setupLayer() {
//        switch gravity {
//
//        case .aspectFill:
//            playerLayer.contentsGravity = .resizeAspectFill
//            playerLayer.videoGravity = .resizeAspectFill
//
//        case .resize:
//            playerLayer.contentsGravity = .resize
//            playerLayer.videoGravity = .resize
//
//        }
//    }
//
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
//    var playerLayer: AVPlayerLayer {
//        return layer as! AVPlayerLayer
//    }
//
//    // Override UIView property
//    override static var layerClass: AnyClass {
//        return AVPlayerLayer.self
//    }
//}
//
//final class PlayerContainerView: UIViewRepresentable {
//    typealias UIViewType = PlayerView
//
//    let player: AVPlayer
//    let gravity: PlayerGravity
//
//    init(player: AVPlayer, gravity: PlayerGravity) {
//        self.player = player
//        self.gravity = gravity
//    }
//
//    func makeUIView(context: Context) -> PlayerView {
//        return PlayerView(player: player, gravity: gravity)
//    }
//
//    func updateUIView(_ uiView: PlayerView, context: Context) { }
//}
//
//class PlayerViewModel: ObservableObject {
//
//    var player: AVPlayer
//
//    init(video_url: String) {
////        let url = Bundle.main.url(forResource: fileName, withExtension: "mp4")
//        self.player =  AVPlayer(
//            url: URL(string: video_url)!
//            )
////        self.play()
//        print("init PlayerViewModel")
//    }
//
//    func seturl(video_url: String){
//        self.player =  AVPlayer(
//            url: URL(string: video_url)!
//            )
//    }
//
//    @Published var isPlaying: Bool = true {
//        didSet {
//            if isPlaying {
//                play()
//            } else {
//                pause()
//            }
//        }
//    }
//
//    func play() {
//        let currentItem = player.currentItem
//        if currentItem?.currentTime() == currentItem?.duration {
//            currentItem?.seek(to: .zero, completionHandler: nil)
//        }
//
//        player.play()
//    }
//
//    func pause() {
//        player.pause()
//    }
//}
//
//enum PlayerAction {
//    case play
//    case pause
//}


//TODO: rename to VideoCardView
struct CardView: View {
    
    var image: String
    var title: String
    var heading: String
    var author: String
    var trimmed_video_url: String
    var video_width: String
    var video_height: String
    var duration: String
    var view_count: String
    let formattedDuration: String
    
    init(
        image: String,
        title: String,
        heading: String,
        author: String,
        trimmed_video_url: String,
        video_width: String,
        video_height: String,
        duration: String,
        view_count: String
    ) {
        self.image = image
        self.title = title
        self.heading = heading
        self.author = author
        self.trimmed_video_url = trimmed_video_url
        self.video_width = video_width
        self.video_height = video_height
        self.duration = duration
        self.view_count = view_count
        
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.unitsStyle = .positional
        formattedDuration = formatter.string(from: TimeInterval(self.duration)!)!
    }
    
    var body: some View {
        let url = URL(string: self.image )
        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        
        let desired_card_width = CGFloat(1080 * 0.3 - 40)
        let calculated_heigth_based_on_ratio = desired_card_width * CGFloat(Int(video_height)!) / CGFloat(Int(video_width)!)
        
        VStack (alignment: .leading, spacing: 0) {
            ZStack (alignment: .bottomTrailing) {
                Image(systemName: "placeholder image")
                    .data(url: URL(string: self.image)!)
                    .resizable()
                    .aspectRatio(contentMode: .fit)

                Text(formattedDuration)
                    .font(.system(size: 8))
                    .padding(5)
                    .background(Color.black)
                    .cornerRadius(40)
                    .foregroundColor(.white)
                    .padding(5)
            }
            
            HStack {
                VStack(alignment: .leading) {
                    Text(self.title)
                        .font(.caption)
                        .foregroundColor(.primary)
                        .fontWeight(.regular)
                    HStack {
                        Text(self.author + "  • " + self.view_count + " visualizzazioni")
                            .font(.caption)
                            .foregroundColor(.secondary)
//                        Text()
//                            .font(.caption)
//                            .foregroundColor(.secondary)
                    }
                    
                }
                .layoutPriority(100)
         
                Spacer()
            }
            .padding(10)
        }
        .background(Color.white)
        .cornerRadius(10)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color(.sRGB, red: 150/255, green: 150/255, blue: 150/255, opacity: 0.1), lineWidth: 1)
        )
    }
    
    

    func loadFileSync(url: URL, completion: @escaping (String?, Error?) -> ())
    {
        let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        let destinationUrl = documentsUrl.appendingPathComponent(url.lastPathComponent)

        if FileManager().fileExists(atPath: destinationUrl.path)
        {
            print("File already exists [\(destinationUrl.path)]")
            completion(destinationUrl.path, nil)
        }
        else if let dataFromURL = NSData(contentsOf: url)
        {
            if dataFromURL.write(to: destinationUrl, atomically: true)
            {
                print("file saved [\(destinationUrl.path)]")
                completion(destinationUrl.path, nil)
            }
            else
            {
                print("error saving file")
                let error = NSError(domain:"Error saving file", code:1001, userInfo:nil)
                completion(destinationUrl.path, error)
            }
        }
        else
        {
            let error = NSError(domain:"Error downloading file", code:1002, userInfo:nil)
            completion(destinationUrl.path, error)
        }
    }

    
}

extension Image {

    func data(url:URL) -> Self {
        if let data = try? Data(contentsOf: url) {
            return Image(uiImage: UIImage(data: data)!)
                .resizable()
        }
        return self
            .resizable()
    }
}

extension UIView {
    func asImage() -> UIImage {
        let format = UIGraphicsImageRendererFormat()
        format.scale = 1
        return UIGraphicsImageRenderer(size: self.layer.frame.size, format: format).image { context in
            self.drawHierarchy(in: self.layer.bounds, afterScreenUpdates: true)
//            layer.render(in: context.cgContext)
        }
    }
}


extension View {
    func asImage(size: CGSize) -> UIImage {
        let controller = UIHostingController(rootView: self)
//        qualcuno su stackoverflow dice che è buggato
//        https://stackoverflow.com/questions/65163097/convert-swiftui-view-to-uiimage-on-ios-14
//        controller.view.bounds = CGRect(origin: .zero, size: size)
        controller.view.bounds = CGRect(origin: CGPoint(x: 0, y: 0.0001), size: size)

        let image = controller.view.asImage()
        return image
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(
            image: "https://i.ytimg.com/vi_webp/lnCXtHhuCko/maxresdefault.webp",
            title: "Quella cosa con la lingua tutorial Quella cosa con la lingua tutorial",
            heading: "",
            author: "Nello Taver Nello Taver",
            trimmed_video_url: "https://tube2story.pythonanywhere.com/static/videos/test_flask.mp4",
            video_width: "1920",
            video_height: "1080",
            duration: "1249",
            view_count: "3490249")
            .frame(width: 1080*0.30 - 40 , alignment: .center)
    }
}
