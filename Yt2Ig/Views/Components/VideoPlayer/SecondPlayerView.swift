//
//  PlayerView.swift
//  VideoPlayer
//
//  Created by Catalin Patrascu on 11.03.2022.
//
//  Distributed under the MIT License

import SwiftUI
import AVFoundation

struct SecondPlayerView: UIViewRepresentable {
    private let fileUrl: URL
    private let player: AVQueuePlayer
    
    init(fileUrl: URL, player: AVQueuePlayer) {
        self.fileUrl = fileUrl
        self.player = player
    }
    
    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<SecondPlayerView>) { }

    func makeUIView(context: Context) -> UIView {
        return LoopingPlayerUIView(fileUrl: fileUrl, player: player)
    }
}
