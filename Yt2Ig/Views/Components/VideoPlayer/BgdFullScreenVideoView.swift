//
//  BgdFullScreenVideoView.swift
//  VideoPlayer
//
//  Created by Catalin Patrascu on 11.03.2022.
//
//  Distributed under the MIT License

import SwiftUI
import AVFoundation

struct BgdFullScreenVideoView: View {
    @State private var player = AVQueuePlayer()
    private let fileUrl: URL
    
    public init(fileUrl: URL) {
        self.fileUrl = fileUrl
    }
    
    public var body: some View {
        SecondPlayerView(fileUrl: fileUrl, player: player)
//                .aspectRatio(contentMode: .fit)
            .frame(width: 1080*0.30, height: 1920*0.30, alignment: .center)
            .onAppear {
                player.play()
            }
            .onDisappear {
                player.pause()
            }
            .onReceive(NotificationCenter.default.publisher(for: UIApplication.willResignActiveNotification)) { _ in
                player.pause()
            }
            .onReceive(NotificationCenter.default.publisher(for: UIApplication.didBecomeActiveNotification)) { _ in
                player.play()
            }
    }
}

struct BackgroundFullScreenVideo_Previews: PreviewProvider {
    static var previews: some View {
        let fileUrl = Bundle.main.url(forResource: "2684b21ee8c94dd3ac19119fb0d4b5a4", withExtension: "MOV")!
        BgdFullScreenVideoView(fileUrl: fileUrl)
    }
}
