//
//  CustomNavBarView.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 09/03/22.
//

import SwiftUI

struct CustomNavBarView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        HStack{
            Button(action: {
                print("custom back button pressed")
                self.presentationMode.wrappedValue.dismiss()
            }, label: {
                
                Image(systemName: "arrow.left")
                    .font(.title3)
                    .foregroundColor(Color.black.opacity(0.7))
                    .frame(width: 50, height: 50)
                    .foregroundColor(Color.black)
                    .background(Color.white)
                    .clipShape(Circle())
                    .shadow(radius: 10)
            })
            
            Spacer()
            
           
            
//            Button(action: {
//                print("custom trailing button pressed")
//
//            }, label: {
//
//                Image(systemName: "arrow.right")
//                    .font(.title3)
//                    .foregroundColor(Color.black.opacity(0.7))
//                    .frame(width: 50, height: 50)
//                    .foregroundColor(Color.black)
//                    .background(Color.yellow)
//                    .clipShape(Circle())
//                    .shadow(radius: 10)
//            })
        }
        .padding()
    }
}

struct CustomNavBarView_Previews: PreviewProvider {
    static var previews: some View {
        CustomNavBarView()
    }
}
