//
//  FirstView.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 02/01/23.
//

import SwiftUI

struct FirstView: View {
    var body: some View {
        TabView {
            StartingView2()
                .tabItem {
                    Image(systemName: "house")
                    Text("Home")
                }
            FilesListView()
                .tabItem {
                    Image(systemName: "list.bullet")
                    Text("History")
                }
        }
    }
}

struct FirstView_Previews: PreviewProvider {
    static var previews: some View {
        FirstView()
    }
}
