//
//  ObjectListView.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 02/01/23.
//

import SwiftUI

struct ObjectListView: View {
    @ObservedObject private var viewModel = ViewModel()
    @State private var isLoading = false

    var body: some View {
        NavigationView{
            ScrollView {
                
                if isLoading {
                    // Show loading spinner
                    Spacer()
                    AnimatedLoaderView()
                } else {
                    LazyVGrid(columns: [GridItem(.adaptive(minimum: 100))]) {
                        ForEach(viewModel.items) { item in
                            NavigationLink(destination: Text("Second View")) {
                                VStack {
                                    ImageView(url: URL(string:item.download_url)!)
                                }
                            }
                        }
                    }
                }
            }.onAppear {
                self.isLoading = true
                self.viewModel.fetchData() {
                    self.isLoading = false
                }
            }
            .navigationTitle("Storielle History")
        }
    }
}

class ViewModel: ObservableObject {
    @Published var items: [CustomItem2] = []

    func fetchData(completion: @escaping () -> Void) {
        let url = URL(string: "http://3.68.124.191:5000/s3_objects")!
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let data = data {
                print("we have dataa")
                print(data)
                let items = try? JSONDecoder().decode([CustomItem2].self, from: data)
                if let items = items {
                    print(items)
                    DispatchQueue.main.async {
                        self.items = items
                        completion()
                    }
                    
                }
            }
        }.resume()
    }

}

struct CustomItem2: Identifiable, Decodable {
    let id: String
    let author: String
    let width: Int
    let height: Int
    let url: String
    let download_url: String
}

struct ImageView: View {
    let url: URL

    var body: some View {
        Image(systemName: "placeholder image")
            .data(url: url)
            .resizable()
            .aspectRatio(contentMode: .fit)
    }
}

struct ObjectListView_Previews: PreviewProvider {
    static var previews: some View {
        ObjectListView ()
    }
}

