//
//  FilesListView.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 07/01/23.
//

import SwiftUI
import AVFoundation

struct FilesListView: View {
    @State var fileURLs: [URL] = []
    @State var selectedURL: URL? = nil
    @State private var whantToBulkDelete = false
    
    let alertTitle: String = "Are you sure you want to delete all your history?"

    var body: some View {
        NavigationView {
            VStack {
                if fileURLs.count != 0 {
                    ScrollView {
                        LazyVGrid(columns: [GridItem(.flexible()), GridItem(.flexible())]) {
                            ForEach(fileURLs, id: \.self) { url in
                                NavigationLink(destination: AssetView(url: url)) {
                                    ThumbnailView(url: url)
                                }
                                .contextMenu {
                                    
                                    Button(action: {
                                        deleteFile(at: url)
                                        self.loadFiles()
                                    }) {
                                        HStack {
                                            Text("Delete")
                                                .foregroundColor(.red)
                                            Image(systemName: "trash")
                                                .foregroundColor(.red)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    NoHistoryView()
                }
            }
            .navigationBarTitle("History")
            .toolbar {
                Button(action: {
                    print("Delete tapped!")
                    whantToBulkDelete = true
                },label: {
                    if fileURLs.count != 0 {
                        Image(systemName: "trash")
                            .foregroundColor(SwiftUI.Color.red)
                        Text("Delete All")
                            .foregroundColor(SwiftUI.Color.red)
                    } else {
                        Image(systemName: "trash")
                        Text("Delete All")
                    }

                })
                .disabled(fileURLs.count == 0)
                .alert(isPresented: $whantToBulkDelete) {
                    Alert(
                        title: Text(alertTitle),
                        primaryButton: .destructive(Text("Delete All"), action: {
                            deleteFiles(type: "documents")
                            loadFiles()
                        }),
                        secondaryButton: .cancel(Text("Nevermind"), action: { // 1
                            print("operation aborted")
                        })
                    )
                }
            }
            .onAppear(perform: loadFiles)
        }
    }

    func loadFiles() {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        do {
            let fileURLs = try FileManager.default.contentsOfDirectory(at: documentsDirectory, includingPropertiesForKeys: nil)
            self.fileURLs = fileURLs.filter { url in
                return url.pathExtension == "mov" || url.pathExtension == "mp4"
            }
            print(self.fileURLs)
        } catch {
            print("Error loading files: \(error)")
        }
    }
}

struct NoHistoryView:View{
    var body: some View{
        Spacer()
        
        Text("To Create Amazing Stories, Go to the Homepage")
            .font(.largeTitle)
            .foregroundColor(SwiftUI.Color.gray)
            .multilineTextAlignment(.center)
            .frame(width: 300)
        
        Spacer()
        
        Text("Get Inspired")
            .foregroundColor(SwiftUI.Color.gray)
            .multilineTextAlignment(.center)
            .frame(width: 300)
        
        Button(
            action: {
                openYoutube()
            },
            label: {
              Label("Open YouTube!", systemImage: "play.rectangle.fill")
                .font(.system(size: 18).bold())
                .padding(6)
                .foregroundColor(.white)
                .background(Color.red)
                .cornerRadius(6)
            })
        
        Spacer()
    }
    
}

extension URL: Identifiable {
    public var id: URL { return self }
}


struct AssetView: View {
    let url: URL

    var body: some View {
        VStack {
            Group {
                if url.pathExtension == "mov" || url.pathExtension == "mp4" {
                    VideoPlayer2(url: url)
                } else if url.pathExtension == "jpg" || url.pathExtension == "png" {
                    Image(uiImage: UIImage(contentsOfFile: url.path)!)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                } else {
                    Text("Unsupported file type")
                        .font(.headline)
                        .foregroundColor(.secondary)
                }
            }
        }
    }
}

struct VideoPlayer2: UIViewRepresentable {
    let url: URL

    func makeUIView(context: Context) -> UIView {
        return PlayerView2(url: url)
    }

    func updateUIView(_ uiView: UIView, context: Context) {
    }
}

class PlayerView2: UIView {
    let playerLayer = AVPlayerLayer()
    let player: AVPlayer
    let playButton = UIButton(type: .system)
    var isPlaying = false
    
    let playImage = UIImage(systemName: "play.fill")
    let pauseImage = UIImage(systemName: "pause.fill")

    init(url: URL) {
        player = AVPlayer(url: url)
        super.init(frame: .zero)

        playerLayer.player = player
        layer.addSublayer(playerLayer)

        playButton.setImage(playImage, for: .normal)
        playButton.addTarget(self, action: #selector(togglePlayback), for: .touchUpInside)
        addSubview(playButton)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        playerLayer.frame = bounds
        playButton.sizeToFit()
        playButton.center = CGPoint(x: bounds.midX, y: bounds.midY)
    }

    @objc func togglePlayback() {
        if isPlaying {
            player.pause()
            playButton.setImage(self.playImage, for: .normal)
        } else {
            player.play()
            playButton.setImage(self.pauseImage, for: .normal)
        }
        isPlaying.toggle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

struct FilesListView_Previews: PreviewProvider {
    static var previews: some View {
        FilesListView()
    }
}
