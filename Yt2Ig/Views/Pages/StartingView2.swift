//
//  StartingView2.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 14/09/21.
//

import SwiftUI

struct HomeView: View{
    // MARK:- PROPERTIES
    @State var scaleEffect1: CGFloat = 0.01
    @State var scaleEffect2: CGFloat = 0.01
    @State var url: String = ""
    
    @State var animationEnded: Bool = false
    
    var body: some View{
        ZStack(){
            
            Image("mesh-gradient")
                .resizable()
                .ignoresSafeArea(edges: [.top, .bottom])
            
            
            VStack{
                Image("newlogo_nobg")
                    .resizable()
                    .frame(width: 200, height: 200, alignment: .center)
                    .scaledToFill()
                
                RatingCardView(scaleEffect1: $scaleEffect1 , scaleEffect2: $scaleEffect2, url: $url )
                    .shadow(color: Color.init(red: 0, green: 0, blue: 0, opacity: 0.1) , radius: 20 , x: 10 , y: 20)
                    .onAnimationCompleted(for: scaleEffect2) {
                        self.animationEnded = true
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            withAnimation(Animation.easeInOut(duration: 0.3)){
                                scaleEffect2 = 0.01
                                scaleEffect1 = 0.01
                            }
                        }
                    }
            }
            .padding(20)
            
            VStack{
                Circle()
                    .fill(Color.init(red: 236/255, green: 61/255, blue: 107/255))
                    .frame(width: 45, height: 45)
                    .scaleEffect(scaleEffect1)
                
                Circle()
                    .fill(Color.init(red: 243/255, green: 163/255, blue: 186/255))
                    .frame(width: 45, height: 45)
                    .scaleEffect(scaleEffect2)
                
                NavigationLink(destination: VincenzoCarouselView(video_url: url), isActive: self.$animationEnded) {
                    Text("")
                }
                .hidden()
            }
        }
    }
}

struct StartingView2: View {
    
    var body: some View {
        NavigationView {
            HomeView()
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
    }
}

/// An animatable modifier that is used for observing animations for a given animatable value.
struct AnimationCompletionObserverModifier<Value>: AnimatableModifier where Value: VectorArithmetic {
    
    /// While animating, SwiftUI changes the old input value to the new target value using this property. This value is set to the old value until the animation completes.
    var animatableData: Value {
        didSet {
            notifyCompletionIfFinished()
        }
    }
    
    /// The target value for which we're observing. This value is directly set once the animation starts. During animation, `animatableData` will hold the oldValue and is only updated to the target value once the animation completes.
    private var targetValue: Value
    
    /// The completion callback which is called once the animation completes.
    private var completion: () -> Void
    
    init(observedValue: Value, completion: @escaping () -> Void) {
        self.completion = completion
        self.animatableData = observedValue
        targetValue = observedValue
    }
    
    /// Verifies whether the current animation is finished and calls the completion callback if true.
    private func notifyCompletionIfFinished() {
        guard animatableData == targetValue else { return }
        
        /// Dispatching is needed to take the next runloop for the completion callback.
        /// This prevents errors like "Modifying state during view update, this will cause undefined behavior."
        DispatchQueue.main.async {
            self.completion()
        }
    }
    
    func body(content: Content) -> some View {
        /// We're not really modifying the view so we can directly return the original input value.
        return content
    }
}

extension View {
    
    /// Calls the completion handler whenever an animation on the given value completes.
    /// - Parameters:
    ///   - value: The value to observe for animations.
    ///   - completion: The completion callback to call once the animation completes.
    /// - Returns: A modified `View` instance with the observer attached.
    func onAnimationCompleted<Value: VectorArithmetic>(for value: Value, completion: @escaping () -> Void) -> ModifiedContent<Self, AnimationCompletionObserverModifier<Value>> {
        return modifier(AnimationCompletionObserverModifier(observedValue: value, completion: completion))
    }
}

struct StartingView2_Previews: PreviewProvider {
    static var previews: some View {
        StartingView2()
            .preferredColorScheme(.dark)
    }
}
