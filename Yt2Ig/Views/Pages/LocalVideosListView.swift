//
//  ObjectListView.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 02/01/23.
//

import SwiftUI

struct LocalVideosListView: View {
    @ObservedObject private var viewModel = LocalVideoViewModel()
    @State private var isLoading = false

    var body: some View {
        NavigationView{
            ScrollView {
                
                if isLoading {
                    // Show loading spinner
                    Spacer()
                    AnimatedLoaderView()
                } else {
                    LazyVGrid(columns: [GridItem(.adaptive(minimum: 100))]) {
                        ForEach(viewModel.items) { item in
                            NavigationLink(destination: Text("Second View")) {
                                Text(item.title)
                            }
                        }
                    }
                }
            }.onAppear {
                self.isLoading = true
                self.viewModel.fetchData() {
                    self.isLoading = false
                }
            }
            .navigationTitle("Storielle History")
        }
    }
}

class LocalVideoViewModel: ObservableObject {
    @Published var items: [LocalVideoItem] = []

    func fetchData(completion: @escaping () -> Void) {
        
        
        DispatchQueue.main.async {
            let fileNames = getFiles(type: "documents")
            for fileName in fileNames {
                self.items.append(LocalVideoItem(id: fileName, title: fileName))
            }
            
            completion()
        }
    }

}

struct LocalVideoItem: Identifiable, Decodable {
    let id: String
    let title: String
}

struct LocalVideosListView_Previews: PreviewProvider {
    static var previews: some View {
        LocalVideosListView ()
    }
}

