//
//  VincenzoCarouselView.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 15/01/21.
//

import SwiftUI
import AVFoundation


struct VincenzoCarouselView: View {
    let video_url: String
    
    var body: some View {
        Home(video_url: self.video_url)
    }
}

struct VincenzoCarouselView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            VincenzoCarouselView(video_url: "https://youtu.be/3EJoX891ZGE")
        }
    }
}

struct Home : View {
    
    @State private var uiimage: UIImage? = nil
    @State var index = 0
    @State var isLoading: Bool = true
    @State var isRendering: Bool = false
    @State var sponsored: Bool = false
    @State var video_is_ready: Bool = false
    @State var local_path: URL = URL(fileURLWithPath: "")
    @State var local_asset: URL = URL(fileURLWithPath: "")
    
    @State var blur_video: URL = URL(fileURLWithPath: "")

    let video_url: String
    
    private let editor = VideoEditor()
    private let blurcontroller = BlurBgController()
    
    @State private var result_video = Video(
        thumbnail: "https://i.ytimg.com/vi_webp/lnCXtHhuCko/maxresdefault.webp",
        title: "QUELLA COSA CON LA LINGUA TUTORIAL",
        uploader: "NELLO TAVER NELLO TAVER",
        trimmed_video_url: "https://tube2story.pythonanywhere.com/static/videos/test_flask.mp4",
        video_width: "1920",
        video_height: "1080",
        duration: "1249",
        view_count: "3490249",
        channel_assets: [""]
    )
    
    var CardView_wrapped: some View {
        CardView(image: result_video.thumbnail,
                       title: result_video.title,
                       heading: "",
                       author: result_video.uploader,
                       trimmed_video_url: result_video.trimmed_video_url,
                       video_width: result_video.video_width,
                       video_height: result_video.video_height,
                       duration: result_video.duration,
                       view_count: result_video.view_count)
            .frame(width: 1080*0.30 - 40 , alignment: .center)
    }
    
    var BottomCardView_wrapped: some View {
        BottomCardView(title: result_video.title,
                       author: result_video.uploader,
                       view_count: result_video.view_count,
                       video_width: result_video.video_width,
                       video_height: result_video.video_height,
                       allCornersRounded: false
        )
            .frame(width: 1080*0.30 - 40 , alignment: .center)
    }
    
    var BottomCardView_wrapped_rounded: some View {
        BottomCardView(title: result_video.title,
                       author: result_video.uploader,
                       view_count: result_video.view_count,
                       video_width: result_video.video_width,
                       video_height: result_video.video_height,
                       allCornersRounded: true
        )
    }
    
    var body: some View{
        
        ZStack{
            
            Color.white.edgesIgnoringSafeArea(.all)
            
            VStack{
                
                CustomNavBarView()
                
                VStack(alignment: .center) {

                    ZStack (alignment: .center) {
                        
                        
                        
                        if !self.isLoading {
                        
                            if self.sponsored {
                                TabView(selection: self.$index){

                                    ForEach(0..<self.result_video.channel_assets.count,id: \.self){index in

                                        VStack {
                                            

                                        }
                                        .frame(width: 1080*0.30, height: self.index == index ? 1920*0.30 : 1920*0.25, alignment: .center)
                                        .background(
                                            Image(uiImage: UIImage(contentsOfFile: self.local_asset.absoluteString)!)
                                                .resizable()
                                                .scaledToFit()
                                        )
                                        .cornerRadius(10)
                                        .shadow(radius: 10)
                                        .tag(index)
                                    }
                                }
                                .padding(.top, -60)
                                .tabViewStyle(PageTabViewStyle())
                                .indexViewStyle(PageIndexViewStyle(backgroundDisplayMode: .always))
                                .animation(.easeOut)

                            }
                            
                            else {
                                TabView(selection: self.$index){

                                    ForEach(0..<myGradients.count,id: \.self){index in

                                        VStack {

                                        }
                                        .frame(width: 1080*0.30, height: self.index == index ? 1920*0.30 : 1920*0.25, alignment: .center)
                                        .background(
                                            // mycolors[index]
                                            myGradients[index]

                                        )
                                        .cornerRadius(10)
                                        .shadow(radius: 10)
                                        .tag(index)
                                    }
                                }
                                .padding(.top, -60)
                                .tabViewStyle(PageTabViewStyle())
                                .indexViewStyle(PageIndexViewStyle(backgroundDisplayMode: .always))
                                .animation(.easeOut)
                            }
                        }
                        
                        VStack{
                            if isLoading {
                                Spacer()
                                ProgressView()
                                    .progressViewStyle(CircularProgressViewStyle(tint: .blue))
                                Spacer()
                            } else {
                                // if video is ready
                                //    VideoCardView
                                // else -> card view con thumbnail
                                if self.video_is_ready
                                {
                                    
                                    VideoCardView(video: result_video)
                                        .frame(width: 1080*0.30 - 40 , alignment: .center)
                                    
                                }
                                else
                                {
                                    ZStack{
                                        CardView_wrapped
                                        VStack{
                                            ProgressView()
                                                .progressViewStyle(CircularProgressViewStyle(tint: .white))
                                            Text("Downloading video...")
                                                .foregroundColor(.white)
                                        }
                                        .padding(20)
                                        .background(Color.gray)
                                        .opacity(0.8)
                                    }

                                }
                            }
                        }

                    }
                    .onAppear(){
                        
                    }
                    .padding(.top, 0)
                }
                
                Button(
                    action: {
                        withAnimation(.spring()){
                            self.isRendering = true
                        }

                        // take card screen that scholud be used as layer to render the final video
                        let videoCardImagesnapshot = BottomCardView_wrapped.asImage
                        let actual_gradient = myGradients[index]
                        
                        let _asset = AVURLAsset(url: self.local_path)
                        let assetTrack = _asset.tracks(withMediaType: .video).first
                        let ogvideosize = assetTrack?.naturalSize

                        var size: CGSize = CGSize(width: 1080, height: 1920)
                        
                        editor.renderVideo(fromVideoAt: self.local_path,
                                           forName: "",
                                           title_image: videoCardImagesnapshot,
                                           gradient_colors: gradient_colors[index],
                                           carousel_background_color: UIColor(mycolors[index]),
                                           background_asset: self.local_asset.absoluteString
                        ) { exportedURL in

                            self.isRendering = false

                            let isPosted = InstaStories.Shared.post(bgVideoUrl: exportedURL!)
                            if(isPosted){
                                // cancellare video originale dalla memoria
                                // tenere video renderizzato dalla memoria
                                // delete file woth url self.local_path
                                deleteFile(at: self.local_path)
                                deleteFiles(type: "temp")
                                deleteFiles(type: "cache")

                                //TODO: here delete on server
                            }
                        }
                },
                    label: {
                        Text("Share to Instagram")
                })
                .frame(minWidth: 0, maxWidth: 300)
                .font(.system(size: 18))
                .padding()
                .background(buttonColor)
                .cornerRadius(40)
                .foregroundColor(.white)
                .disabled(self.isRendering || !self.video_is_ready)
                .opacity(buttonOpacity)
            }
        }
        .navigationBarTitle("Hidden Title")
        .navigationBarHidden(true)
        .overlay(
            ZStack{
                if isRendering {
                    Color.primary.opacity(0.3)
                        .ignoresSafeArea()
                    AnimatedLoaderView()
                        .offset(y: isRendering ? 0 : UIScreen.main.bounds.height)
                }
            }
        )
        .onAppear(){
            postRequest(urlString: self.video_url) {result in
                // facciamo la chiamata iniziale

                switch result {
                case .success(let _video):
                    // qui abbiamo scaricato i metadati

                    self.result_video = _video
                    
                    if(self.result_video.channel_assets.count != 0){
                        print("there are assets")
                        for filename in self.result_video.channel_assets{
                            print("asset filename \(filename)")
                            
                            loadFileAsync(url: URL(string: "https://storielle-assets.s3.eu-central-1.amazonaws.com/\(filename)")!) { (path, error) in
                                
                                let __url = URL(string: path!)
                                print("asset local path : \(__url!)")

                                // set
                                self.local_asset = __url!

                                // il video in locale è pronto
                                self.sponsored = true
                            }
                        }
                        
                    }
                    
                    self.isLoading = false

                    let video_id = _video.trimmed_video_url.components(separatedBy: "download/")[1].components(separatedBy: ".m")[0]

                    print("is trimming")
                    
                    let __baseurl = "http://3.68.124.191:5000"
//                    var __baseurl = "http://192.168.1.48:5000"
                    // controlliamo "video_exist" finche il video non è stato tagliato e ci ritorna 200
                    fetchWithRetry(url: __baseurl + "/video_exist/" + video_id) { data, text in
                        print("video trimmed. downloading...")
                        // qui potremmo scaricare il video con load_async e poi usare sempre il file locale
                        loadFileAsync(url: URL(string: result_video.trimmed_video_url)!) { (path, error) in
                            print("video File downloaded to : \(path!)")
                            let __url = URL(string: "file:///private"+path!)

                            // set
                            self.local_path = __url!

                            // il video in locale è pronto
                            self.video_is_ready = true
                        }
                    }
                case .failure(let error):
                    switch error {
                    // TODO: address this situations. alert or something else
                    case .badURL:
                        print("Bad URL")
                    case .requestFailed:
                        print("Network problems")
                    case .unknown:
                        print("Unknown error")
                    }
                }
            }
        }
    }
    
    var buttonOpacity: Double {
        return (self.isRendering || !self.video_is_ready) ? 0.7 : 1.0
    }
    
    var buttonColor: LinearGradient {
        return (self.isRendering || !self.video_is_ready) ?
            LinearGradient(gradient: Gradient(colors: [.gray]), startPoint: .leading, endPoint: .trailing) :
            LinearGradient(gradient: Gradient(colors: [Color("Color1"), Color("Color2"), Color("Color3"), Color("Color4"), Color("Color5")]), startPoint: .leading, endPoint: .trailing)
    }
    
    func hexStringFromColor(color: UIColor) -> String {
        let components = color.cgColor.components
        let r: CGFloat = components?[0] ?? 0.0
        let g: CGFloat = components?[1] ?? 0.0
        let b: CGFloat = components?[2] ?? 0.0
        
        let hexString = String.init(format: "#%02lX%02lX%02lX", lroundf(Float(r * 255)), lroundf(Float(g * 255)), lroundf(Float(b * 255)))
        print(hexString)
        return hexString
    }
}

extension UINavigationController: UIGestureRecognizerDelegate {
    override open func viewDidLoad() {
        super.viewDidLoad()
        interactivePopGestureRecognizer?.delegate = self
    }

    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return viewControllers.count > 1
    }
}

extension UIImage {
    // image with rounded corners
    public func withRoundedCorners(radius: CGFloat? = nil) -> UIImage? {
        let maxRadius = min(size.width, size.height) / 2
        let cornerRadius: CGFloat
        if let radius = radius, radius > 0 && radius <= maxRadius {
            cornerRadius = radius
        } else {
            cornerRadius = maxRadius
        }
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        let rect = CGRect(origin: .zero, size: size)
        UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius).addClip()
        draw(in: rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

func hexStringToUIColor (hex:String) -> Color {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return Color.gray
    }
    
    var rgbValue:UInt64 = 0
    Scanner(string: cString).scanHexInt64(&rgbValue)
    
    return Color(UIColor(
                    red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                    green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                    blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                    alpha: CGFloat(1.0))
    )
}

// TODO: find definitive colors
var mycolors = [
    Color.white,
    Color("Color1"),
    Color("Color2"),
    Color("Color3"),
    Color("Color4"),
    Color("Color5"),
    Color("Color4"),
    Color("Color5")]

var myGradients = [
    LinearGradient(gradient: Gradient(colors: [
                                        hexStringToUIColor(hex: "f9c58d"),
                                        hexStringToUIColor(hex: "f492f0")]), startPoint: .top, endPoint: .bottom),
    LinearGradient(gradient: Gradient(colors: [
                                        hexStringToUIColor(hex: "f6d5f7"),
                                        hexStringToUIColor(hex: "fbe9d7")]), startPoint: .top, endPoint: .bottom),
    LinearGradient(gradient: Gradient(colors: [
                                        hexStringToUIColor(hex: "84ffc9"),
                                        hexStringToUIColor(hex: "aab2ff"),
                                        hexStringToUIColor(hex: "eca0ff")]),
                   startPoint: .top, endPoint: .bottom),
    LinearGradient(gradient: Gradient(colors: [
                                        hexStringToUIColor(hex: "ffffff"),
                                        hexStringToUIColor(hex: "ffffff")]), startPoint: .top, endPoint: .bottom),
    LinearGradient(gradient: Gradient(colors: [
                                        hexStringToUIColor(hex: "000000"),
                                        hexStringToUIColor(hex: "000000")]), startPoint: .top, endPoint: .bottom)
]

var gradient_colors = [
    [UIColor(hexStringToUIColor(hex: "f9c58d")), UIColor(hexStringToUIColor(hex: "f492f0"))],
    [UIColor(hexStringToUIColor(hex: "f6d5f7")), UIColor(hexStringToUIColor(hex: "fbe9d7"))],
    [UIColor(hexStringToUIColor(hex: "84ffc9")), UIColor(hexStringToUIColor(hex: "aab2ff")), UIColor(hexStringToUIColor(hex: "eca0ff"))],
    [UIColor(hexStringToUIColor(hex: "ffffff")), UIColor(hexStringToUIColor(hex: "ffffff"))],
    [UIColor(hexStringToUIColor(hex: "000000")), UIColor(hexStringToUIColor(hex: "000000"))],
]


//pro version
var myImages = [UIImage(named: "background_test")]


