//
//  TestView.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 26/02/22.
//

import SwiftUI

struct TestView: View {
    @EnvironmentObject var dm: DownloadManager

    var body: some View {
        VStack(spacing: 40) {
            DownloadButton()

            if dm.isDownloaded {
                Text("file ready")
//                WatchButton()
//                    .onTapGesture {
//                        showVideo = true
//                    }
//                    .fullScreenCover(isPresented: $showVideo, content: {
//                        VideoView()
//                    })
            }
        }
        .padding(.horizontal, 20)
//        .onAppear {
//            dm.checkFileExists()
//        }
    }
}

//struct TestView_Previews: PreviewProvider {
//    static var previews: some View {
//        TestView()
//    }
//}
