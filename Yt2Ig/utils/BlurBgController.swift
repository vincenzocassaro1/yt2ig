//
//  BlurBgController.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 11/06/22.
//

import Foundation

import UIKit
import AVFoundation
import Photos

internal func RadiansToDegree(radians: CGFloat) -> CGFloat {
    return (radians * 180.0)/(CGFloat)(Double.pi)
}

class BlurBgController {
    
    var videos : Array<AVURLAsset> = []
    var size: CGSize = CGSize(width: 1080, height: 1920)
    
    
    // MARK:- Methods
    func startProcess() {
        
        self.mergeVideos(videos, inArea: size) { (error, videoURL) in
            
            if let errorInMerging = error {
                self.processWithMessage(message: errorInMerging.localizedDescription)
                return
            }
            
            /// Adding blur effect to whole video
            guard let url = videoURL else {
                self.processWithMessage(message: "no video url found")
                return
            }
            
            let asset = AVURLAsset(url: url)
            self.addBlurEffect(toVideo: asset, completion: { (error2, videoURL2) in
                
                if let errorInBlur = error2 {
                    self.processWithMessage(message: errorInBlur.localizedDescription)
                    return
                }
                
                /// Adding blur effect to whole video
                guard let blurUrl = videoURL2 else {
                    self.processWithMessage(message: "no video url found")
                    return
                }
                
                let blurAsset = AVURLAsset(url: blurUrl)
                self.addAllVideosAtCenterOfBlur(videos: self.videos, blurVideo: blurAsset, completion: {(error3, videoURL3) in
                    if let errorInFinalVideo = error3 {
                        self.processWithMessage(message: errorInFinalVideo.localizedDescription)
                        return
                    }
                    
                    /// Adding blur effect to whole video
                    guard let finalVideoUrl = videoURL3 else {
                        self.processWithMessage(message: "no video url found")
                        return
                    }
                    
                    self.saveVideo(videoURL: finalVideoUrl)
                })
            })
        }
    }
    
    func processWithMessage(message: String) {
        print(message)
//        let alertController = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
//        let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
//            self.dismiss(animated: true, completion: {
//
//            })
//        }
//        alertController.addAction(alertAction)
//        self.present(alertController, animated: true) { }
    }
    
    func applicationDirectory() -> String {
        return NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
    }
    
    var videoOutputURL: URL {
        let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0] + "/video.mp4"
        return URL(fileURLWithPath: path)
    }
    
    func assetSize(forTrack videoTrack:AVAssetTrack) -> CGSize {
        let size = videoTrack.naturalSize.applying(videoTrack.preferredTransform)
        return CGSize(width: abs(size.width), height: abs(size.height))
    }
    
    func getVideoOrientation(forTrack videoTrack:AVAssetTrack) -> UIImage.Orientation {
        let txf: CGAffineTransform = videoTrack.preferredTransform
        let videoAngleInDegree: CGFloat = RadiansToDegree(radians: atan2(txf.b, txf.a))
        var orientation: UIImage.Orientation = .up
        switch (Int)(videoAngleInDegree) {
        case 0:
            orientation = .right
            break
        case 90:
            orientation = .up
            break
        case 180:
            orientation = .left
            break
        case -90:
            orientation = .down
            break
        default:
            orientation = .up
            break
        }
        return orientation
    }
    
    func totalTime(forVideos videos:Array<AVURLAsset>) -> CMTime {
        var time = CMTime.zero
        for asset in videos {
            time = CMTimeAdd(time, asset.duration)
        }
        return time
    }
    
    
    func scaleAndPositionInAspectFillMode(forTrack track:AVAssetTrack, inArea area: CGSize) -> (scale: CGSize, position: CGPoint) {
        let assetSize = self.assetSize(forTrack: track)
        let aspectFillSize  = CGSize.aspectFill(videoSize: assetSize, boundingSize: area)
        let aspectFillScale = CGSize(width: aspectFillSize.width/assetSize.width, height: aspectFillSize.height/assetSize.height)
        let position = CGPoint(x: (area.width - aspectFillSize.width)/2.0, y: (area.height - aspectFillSize.height)/2.0)
        return (scale: aspectFillScale, position: position)
    }
    
    func scaleAndPositionInAspectFitMode(forTrack track:AVAssetTrack, inArea area: CGSize) -> (scale: CGSize, position: CGPoint) {
        let assetSize = self.assetSize(forTrack: track)
        let aspectFitSize  = CGSize.aspectFit(videoSize: assetSize, boundingSize: area)
        let aspectFitScale = CGSize(width: aspectFitSize.width/assetSize.width, height: aspectFitSize.height/assetSize.height)
        let position = CGPoint(x: (area.width - aspectFitSize.width)/2.0, y: (area.height - aspectFitSize.height)/2.0)
        return (scale: aspectFitScale, position: position)
    }
    
    // MARK:- Main Methods
    func mergeVideos(_ videos: Array<AVURLAsset>, inArea area:CGSize, completion: @escaping (_ error: Error?, _ url:URL?) -> Swift.Void) {
        
        // Create AVMutableComposition Object.This object will hold our multiple AVMutableCompositionTrack.
        let mixComposition = AVMutableComposition()
        
        var instructionLayers : Array<AVMutableVideoCompositionLayerInstruction> = []
        
        for asset in videos {
            
            // Here we are creating the AVMutableCompositionTrack. See how we are adding a new track to our AVMutableComposition.
            let track = mixComposition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: kCMPersistentTrackID_Invalid)
            
            // Now we set the length of the track equal to the length of the asset and add the asset to out newly created track at kCMTimeZero for first track and lastAssetTime for current track so video plays from the start of the track to end.
            if let videoTrack = asset.tracks(withMediaType: AVMediaType.video).first {
                
                
                /// Hide time for this video's layer
                let opacityStartTime: CMTime = CMTimeMakeWithSeconds(0, preferredTimescale: asset.duration.timescale)
                let opacityEndTime: CMTime = CMTimeAdd(mixComposition.duration, asset.duration)
                let hideAfter: CMTime = CMTimeAdd(opacityStartTime, opacityEndTime)
                
                
                let timeRange = CMTimeRangeMake(start: CMTime.zero, duration: asset.duration)
                try? track?.insertTimeRange(timeRange, of: videoTrack, at: mixComposition.duration)
                
                
                /// Layer instrcution
                let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track!)
                layerInstruction.setOpacity(0.0, at: hideAfter)
                
                /// Add logic for aspectFit in given area
                let properties = scaleAndPositionInAspectFillMode(forTrack: videoTrack, inArea: area)
                
                
                /// Checking for orientation
                let videoOrientation: UIImage.Orientation = self.getVideoOrientation(forTrack: videoTrack)
                let assetSize = self.assetSize(forTrack: videoTrack)
                
                if (videoOrientation == .down) {
                    /// Rotate
                    let defaultTransfrom = asset.preferredTransform
                    let rotateTransform = CGAffineTransform(rotationAngle: -CGFloat(Double.pi/2.0))
                    
                    // Scale
                    let scaleTransform = CGAffineTransform(scaleX: properties.scale.width, y: properties.scale.height)
                    
                    // Translate
                    var ytranslation: CGFloat = assetSize.height
                    var xtranslation: CGFloat = 0
                    if properties.position.y == 0 {
                        xtranslation = -(assetSize.width - ((size.width/size.height) * assetSize.height))/2.0
                    }
                    else {
                        ytranslation = assetSize.height - (assetSize.height - ((size.height/size.width) * assetSize.width))/2.0
                    }
                    let translationTransform = CGAffineTransform(translationX: xtranslation, y: ytranslation)
                    
                    // Final transformation - Concatination
                    let finalTransform = defaultTransfrom.concatenating(rotateTransform).concatenating(translationTransform).concatenating(scaleTransform)
                    layerInstruction.setTransform(finalTransform, at: CMTime.zero)
                }
                else if (videoOrientation == .left) {
                    
                    /// Rotate
                    let defaultTransfrom = asset.preferredTransform
                    let rotateTransform = CGAffineTransform(rotationAngle: -CGFloat(Double.pi))
                    
                    // Scale
                    let scaleTransform = CGAffineTransform(scaleX: properties.scale.width, y: properties.scale.height)
                    
                    // Translate
                    var ytranslation: CGFloat = assetSize.height
                    var xtranslation: CGFloat = assetSize.width
                    if properties.position.y == 0 {
                        xtranslation = assetSize.width - (assetSize.width - ((size.width/size.height) * assetSize.height))/2.0
                    }
                    else {
                        ytranslation = assetSize.height - (assetSize.height - ((size.height/size.width) * assetSize.width))/2.0
                    }
                    let translationTransform = CGAffineTransform(translationX: xtranslation, y: ytranslation)
                    
                    // Final transformation - Concatination
                    let finalTransform = defaultTransfrom.concatenating(rotateTransform).concatenating(translationTransform).concatenating(scaleTransform)
                    layerInstruction.setTransform(finalTransform, at: CMTime.zero)
                }
                else if (videoOrientation == .right) {
                    /// No need to rotate
                    // Scale
                    let scaleTransform = CGAffineTransform(scaleX: properties.scale.width, y: properties.scale.height)
                    
                    // Translate
                    let translationTransform = CGAffineTransform(translationX: properties.position.x, y: properties.position.y)
                    
                    let finalTransform  = scaleTransform.concatenating(translationTransform)
                    layerInstruction.setTransform(finalTransform, at: CMTime.zero)
                }
                else {
                    /// Rotate
                    let defaultTransfrom = asset.preferredTransform
                    let rotateTransform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2.0))
                    
                    // Scale
                    let scaleTransform = CGAffineTransform(scaleX: properties.scale.width, y: properties.scale.height)
                    
                    // Translate
                    var ytranslation: CGFloat = 0
                    var xtranslation: CGFloat = assetSize.width
                    if properties.position.y == 0 {
                        xtranslation = assetSize.width - (assetSize.width - ((size.width/size.height) * assetSize.height))/2.0
                    }
                    else {
                        ytranslation = -(assetSize.height - ((size.height/size.width) * assetSize.width))/2.0
                    }
                    let translationTransform = CGAffineTransform(translationX: xtranslation, y: ytranslation)
                    
                    // Final transformation - Concatination
                    let finalTransform = defaultTransfrom.concatenating(rotateTransform).concatenating(translationTransform).concatenating(scaleTransform)
                    layerInstruction.setTransform(finalTransform, at: CMTime.zero)
                }
                
                instructionLayers.append(layerInstruction)
            }
        }
        
        
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: mixComposition.duration)
        mainInstruction.layerInstructions = instructionLayers
        
        let mainCompositionInst = AVMutableVideoComposition()
        mainCompositionInst.instructions = [mainInstruction]
        mainCompositionInst.frameDuration = CMTimeMake(value: 1, timescale: 30)
        mainCompositionInst.renderSize = area
        
        //let url = URL(fileURLWithPath: "/Users/enacteservices/Desktop/final_video.mov")
        let url = self.videoOutputURL
        try? FileManager.default.removeItem(at: url)
        
        let exporter = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)
        exporter?.outputURL = url
        exporter?.outputFileType = .mp4
        exporter?.videoComposition = mainCompositionInst
        exporter?.shouldOptimizeForNetworkUse = true
        exporter?.exportAsynchronously(completionHandler: {
            if let anError = exporter?.error {
                completion(anError, nil)
            }
            else if exporter?.status == AVAssetExportSession.Status.completed {
                completion(nil, url)
            }
        })
    }
    
    func addBlurEffect(toVideo asset:AVURLAsset, completion: @escaping (_ error: Error?, _ url:URL?) -> Swift.Void) {
        
        let filter = CIFilter(name: "CIGaussianBlur")
        let composition = AVVideoComposition(asset: asset, applyingCIFiltersWithHandler: { request in
            // Clamp to avoid blurring transparent pixels at the image edges
            let source: CIImage? = request.sourceImage.clampedToExtent()
            filter?.setValue(source, forKey: kCIInputImageKey)
            
            filter?.setValue(10.0, forKey: kCIInputRadiusKey)
            
            // Crop the blurred output to the bounds of the original image
            let output: CIImage? = filter?.outputImage?.cropped(to: request.sourceImage.extent)
            
            // Provide the filter output to the composition
            if let anOutput = output {
                request.finish(with: anOutput, context: nil)
            }
        })
        
        //let url = URL(fileURLWithPath: "/Users/enacteservices/Desktop/final_video.mov")
        let url = self.videoOutputURL
        // Remove any prevouis videos at that path
        try? FileManager.default.removeItem(at: url)
        
        let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)
        
        // assign all instruction for the video processing (in this case the transformation for cropping the video
        exporter?.videoComposition = composition
        exporter?.outputFileType = .mp4
        exporter?.outputURL = url
        exporter?.exportAsynchronously(completionHandler: {
            if let anError = exporter?.error {
                completion(anError, nil)
            }
            else if exporter?.status == AVAssetExportSession.Status.completed {
                completion(nil, url)
            }
        })
    }
    
    func addAllVideosAtCenterOfBlur(videos: Array<AVURLAsset>, blurVideo: AVURLAsset, completion: @escaping (_ error: Error?, _ url:URL?) -> Swift.Void) {
        
        
        // Create AVMutableComposition Object.This object will hold our multiple AVMutableCompositionTrack.
        let mixComposition = AVMutableComposition()
        
        var instructionLayers : Array<AVMutableVideoCompositionLayerInstruction> = []
        
        
        // Add blur video first
        let blurVideoTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: kCMPersistentTrackID_Invalid)
        // Blur layer instruction
        if let videoTrack = blurVideo.tracks(withMediaType: AVMediaType.video).first {
            let timeRange = CMTimeRangeMake(start: CMTime.zero, duration: blurVideo.duration)
            try? blurVideoTrack?.insertTimeRange(timeRange, of: videoTrack, at: CMTime.zero)
        }
        
        /// Add other videos at center of the blur video
        var startAt = CMTime.zero
        for asset in videos {
            
            /// Time Range of asset
            let timeRange = CMTimeRangeMake(start: CMTime.zero, duration: asset.duration)
            
            // Here we are creating the AVMutableCompositionTrack. See how we are adding a new track to our AVMutableComposition.
            let track = mixComposition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: kCMPersistentTrackID_Invalid)
            
            // Now we set the length of the track equal to the length of the asset and add the asset to out newly created track at kCMTimeZero for first track and lastAssetTime for current track so video plays from the start of the track to end.
            if let videoTrack = asset.tracks(withMediaType: AVMediaType.video).first {
                
                /// Hide time for this video's layer
                let opacityStartTime: CMTime = CMTimeMakeWithSeconds(0, preferredTimescale: asset.duration.timescale)
                let opacityEndTime: CMTime = CMTimeAdd(startAt, asset.duration)
                let hideAfter: CMTime = CMTimeAdd(opacityStartTime, opacityEndTime)
                
                /// Adding video track
                try? track?.insertTimeRange(timeRange, of: videoTrack, at: startAt)
                
                /// Layer instrcution
                let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track!)
                layerInstruction.setOpacity(0.0, at: hideAfter)
                
                /// Add logic for aspectFit in given area
                let properties = scaleAndPositionInAspectFitMode(forTrack: videoTrack, inArea: size)
                
                /// Checking for orientation
                let videoOrientation: UIImage.Orientation = self.getVideoOrientation(forTrack: videoTrack)
                let assetSize = self.assetSize(forTrack: videoTrack)
                
                if (videoOrientation == .down) {
                    /// Rotate
                    let defaultTransfrom = asset.preferredTransform
                    let rotateTransform = CGAffineTransform(rotationAngle: -CGFloat(Double.pi/2.0))
                    
                    // Scale
                    let scaleTransform = CGAffineTransform(scaleX: properties.scale.width, y: properties.scale.height)
                    
                    // Translate
                    var ytranslation: CGFloat = assetSize.height
                    var xtranslation: CGFloat = 0
                    if properties.position.y == 0 {
                        xtranslation = -(assetSize.width - ((size.width/size.height) * assetSize.height))/2.0
                    }
                    else {
                        ytranslation = assetSize.height - (assetSize.height - ((size.height/size.width) * assetSize.width))/2.0
                    }
                    let translationTransform = CGAffineTransform(translationX: xtranslation, y: ytranslation)
                    
                    // Final transformation - Concatination
                    let finalTransform = defaultTransfrom.concatenating(rotateTransform).concatenating(translationTransform).concatenating(scaleTransform)
                    layerInstruction.setTransform(finalTransform, at: CMTime.zero)
                }
                else if (videoOrientation == .left) {
                    
                    /// Rotate
                    let defaultTransfrom = asset.preferredTransform
                    let rotateTransform = CGAffineTransform(rotationAngle: -CGFloat(Double.pi))
                    
                    // Scale
                    let scaleTransform = CGAffineTransform(scaleX: properties.scale.width, y: properties.scale.height)
                    
                    // Translate
                    var ytranslation: CGFloat = assetSize.height
                    var xtranslation: CGFloat = assetSize.width
                    if properties.position.y == 0 {
                        xtranslation = assetSize.width - (assetSize.width - ((size.width/size.height) * assetSize.height))/2.0
                    }
                    else {
                        ytranslation = assetSize.height - (assetSize.height - ((size.height/size.width) * assetSize.width))/2.0
                    }
                    let translationTransform = CGAffineTransform(translationX: xtranslation, y: ytranslation)
                    
                    // Final transformation - Concatination
                    let finalTransform = defaultTransfrom.concatenating(rotateTransform).concatenating(translationTransform).concatenating(scaleTransform)
                    layerInstruction.setTransform(finalTransform, at: CMTime.zero)
                }
                else if (videoOrientation == .right) {
                    /// No need to rotate
                    // Scale
                    let scaleTransform = CGAffineTransform(scaleX: properties.scale.width, y: properties.scale.height)
                    
                    // Translate
                    let translationTransform = CGAffineTransform(translationX: properties.position.x, y: properties.position.y)
                    
                    let finalTransform  = scaleTransform.concatenating(translationTransform)
                    layerInstruction.setTransform(finalTransform, at: CMTime.zero)
                }
                else {
                    /// Rotate
                    let defaultTransfrom = asset.preferredTransform
                    let rotateTransform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2.0))
                    
                    // Scale
                    let scaleTransform = CGAffineTransform(scaleX: properties.scale.width, y: properties.scale.height)
                    
                    // Translate
                    var ytranslation: CGFloat = 0
                    var xtranslation: CGFloat = assetSize.width
                    if properties.position.y == 0 {
                        xtranslation = assetSize.width - (assetSize.width - ((size.width/size.height) * assetSize.height))/2.0
                    }
                    else {
                        ytranslation = -(assetSize.height - ((size.height/size.width) * assetSize.width))/2.0
                    }
                    let translationTransform = CGAffineTransform(translationX: xtranslation, y: ytranslation)
                    
                    // Final transformation - Concatination
                    let finalTransform = defaultTransfrom.concatenating(rotateTransform).concatenating(translationTransform).concatenating(scaleTransform)
                    layerInstruction.setTransform(finalTransform, at: CMTime.zero)
                }
                
                instructionLayers.append(layerInstruction)
            }
            
            /// Adding audio
            if let audioTrack = asset.tracks(withMediaType: AVMediaType.audio).first {
                let aTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)
                try? aTrack?.insertTimeRange(timeRange, of: audioTrack, at: startAt)
            }
            
            // Increase the startAt time
            startAt = CMTimeAdd(startAt, asset.duration)
        }
        
        
        /// Blur layer instruction
        let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: blurVideoTrack!)
        instructionLayers.append(layerInstruction)
        
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: blurVideo.duration)
        mainInstruction.layerInstructions = instructionLayers
        
        let mainCompositionInst = AVMutableVideoComposition()
        mainCompositionInst.instructions = [mainInstruction]
        mainCompositionInst.frameDuration = CMTimeMake(value: 1, timescale: 30)
        mainCompositionInst.renderSize = size
        
        //let url = URL(fileURLWithPath: "/Users/enacteservices/Desktop/final_video.mov")
        let url = self.videoOutputURL
        try? FileManager.default.removeItem(at: url)
        
        let exporter = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)
        exporter?.outputURL = url
        exporter?.outputFileType = .mp4
        exporter?.videoComposition = mainCompositionInst
        exporter?.shouldOptimizeForNetworkUse = true
        exporter?.exportAsynchronously(completionHandler: {
            if let anError = exporter?.error {
                completion(anError, nil)
            }
            else if exporter?.status == AVAssetExportSession.Status.completed {
                completion(nil, url)
            }
        })
    }
    
    // MARK:- Save Video to Camera Roll
    func saveVideo(videoURL: URL) {
        PHPhotoLibrary.requestAuthorization({ status in
            if status == .authorized {
                self.saveVideo(toAlbum: videoURL) { error, success in
                    if let _error = error {
                        self.processWithMessage(message: _error.localizedDescription)
                    }
                    else {
                        self.processWithMessage(message: "Saved to camera roll.")
                    }
                }
            } else {
                self.processWithMessage(message: "Unable to save to camera roll.")
            }
        })
    }
    
    func saveVideo(toAlbum videoURL: URL, completion block: @escaping (_ error: Error?, _ success: Bool) -> Void) {
        
        let PHOTO_ALBUM_NAME = "BlurBG"
        var collection = findAlbumAssetCollection(PHOTO_ALBUM_NAME)
        if collection == nil {
            
            // Collection Album not found - Create Folder
            PHPhotoLibrary.shared().performChanges({
                
                // Get a placeholder for the new asset and add it to the album editing request.
                _ = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: PHOTO_ALBUM_NAME).placeholderForCreatedAssetCollection
                
            }, completionHandler: { success, error in
                
                if success {
                    // Created successfully
                    PHPhotoLibrary.shared().performChanges({
                        let placeholder: PHObjectPlaceholder = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoURL)!.placeholderForCreatedAsset!
                        collection = self.findAlbumAssetCollection(PHOTO_ALBUM_NAME)
                        let photosAsset = PHAsset.fetchAssets(in: collection!, options: nil)
                        let albumChangeRequest = PHAssetCollectionChangeRequest(for: collection!, assets: photosAsset)
                        
                        albumChangeRequest?.addAssets([placeholder] as NSArray)
                        
                    }, completionHandler: { success, error in
                        block(error, success)
                    })
                } else {
                    block(error, success)
                }
            })
        }
        else {
            PHPhotoLibrary.shared().performChanges({
                let placeholder: PHObjectPlaceholder = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoURL)!.placeholderForCreatedAsset!
                let photosAsset = PHAsset.fetchAssets(in: collection!, options: nil) as PHFetchResult<PHAsset>
                let albumChangeRequest = PHAssetCollectionChangeRequest(for: collection!, assets: photosAsset)
                albumChangeRequest?.addAssets([placeholder] as NSArray)
                
            }, completionHandler: { success, error in
                block(error, success)
            })
        }
    }
    
    func findAlbumAssetCollection(_ albumName: String) -> PHAssetCollection? {
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", albumName)
        let collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions).firstObject
        return collection
    }
    
    // MARK:- TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if let text = textField.text {
            let array = text.components(separatedBy: ",")
            if array.count == 2, let w = NumberFormatter().number(from: array[0]), let h = NumberFormatter().number(from: array[1]) {
                let width = CGFloat(w.floatValue)
                let height = CGFloat(h.floatValue)
                size = CGSize(width: width, height: height)
            }
        }
        
        textField.resignFirstResponder()
        return true
    }
    
}


extension CGSize {
    
    static func aspectFit(videoSize: CGSize, boundingSize: CGSize) -> CGSize {
        
        var size = boundingSize
        let mW = boundingSize.width / videoSize.width;
        let mH = boundingSize.height / videoSize.height;
        
        if( mH < mW ) {
            size.width = boundingSize.height / videoSize.height * videoSize.width;
        }
        else if( mW < mH ) {
            size.height = boundingSize.width / videoSize.width * videoSize.height;
        }
        
        return size;
    }
    
    static func aspectFill(videoSize: CGSize, boundingSize: CGSize) -> CGSize {
        
        var size = boundingSize
        let mW = boundingSize.width / videoSize.width;
        let mH = boundingSize.height / videoSize.height;
        
        if( mH > mW ) {
            size.width = boundingSize.height / videoSize.height * videoSize.width;
        }
        else if ( mW > mH ) {
            size.height = boundingSize.width / videoSize.width * videoSize.height;
        }
        
        return size;
    }
}

/*extension UIImage {
 
 static func from(color: UIColor) -> UIImage {
 let rect = CGRect(x: 0, y: 0, width: 100, height: 100)
 UIGraphicsBeginImageContextWithOptions(rect.size, false, 3.0)
 let context = UIGraphicsGetCurrentContext()
 context!.setFillColor(color.cgColor)
 context!.fill(rect)
 let img = UIGraphicsGetImageFromCurrentImageContext()
 UIGraphicsEndImageContext()
 return img!
 }
 
 func blur() -> UIImage {
 
 let context = CIContext(options: nil)
 let currentFilter = CIFilter(name: "CIGaussianBlur")
 let beginImage = CIImage(image: self)
 currentFilter!.setValue(beginImage, forKey: kCIInputImageKey)
 currentFilter!.setValue(100, forKey: kCIInputRadiusKey)
 
 let cropFilter = CIFilter(name: "CICrop")
 cropFilter!.setValue(currentFilter!.outputImage, forKey: kCIInputImageKey)
 cropFilter!.setValue(CIVector(cgRect: beginImage!.extent), forKey: "inputRectangle")
 
 let output = cropFilter!.outputImage
 let cgimg = context.createCGImage(output!, from: output!.extent)
 let processedImage = UIImage(cgImage: cgimg!)
 return processedImage
 }
 }*/

