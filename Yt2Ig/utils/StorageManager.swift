//
//  Storage.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 09/03/22.
//

import Foundation

func listFiles(type: String){
    cicleFiles(type: type, delete: false)
}

func deleteFiles(type: String){
    cicleFiles(type: type, delete: true)
}

func getFiles(type: String) -> [String]{
    var documentsUrl: NSURL
    let fileManager = FileManager.default
    
    switch type {
    case "documents":
        documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
    case "temps":
        documentsUrl =  FileManager.default.temporaryDirectory as NSURL
    default:
        documentsUrl =  FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first! as NSURL
    }
    
    
    let documentsPath = documentsUrl.path
    do {
        if let documentPath = documentsPath
        {
            let fileNames = try fileManager.contentsOfDirectory(atPath: "\(documentPath)")
            print(fileNames)
            return fileNames
        }
    } catch {
        print("Could not list \(type) folder: \(error)")
    }
    
    return []
}

func deleteFile(at fileURL: URL) {
    let fileManager = FileManager.default
    do {
        try fileManager.removeItem(at: fileURL)
        print("File deleted")
    } catch {
        print("Error deleting file: \(error)")
    }
}


private func cicleFiles(type: String, delete: Bool){
    
    var documentsUrl: NSURL
    let fileManager = FileManager.default
    
    switch type {
    case "documents":
        documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
    case "temps":
        documentsUrl =  FileManager.default.temporaryDirectory as NSURL
    default:
        documentsUrl =  FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first! as NSURL
    }
    
    
    let documentsPath = documentsUrl.path
    
    do {
        if let documentPath = documentsPath
        {
            let fileNames = try fileManager.contentsOfDirectory(atPath: "\(documentPath)")
            //print("documentPath", documentPath)
            //print("all files in " + type + ": \(fileNames)")
            for fileName in fileNames {
                let filePathName = "\(documentPath)/\(fileName)"
                print("Element found -> \(filePathName)")
                
                if delete {
                    if (fileName.hasSuffix(".mp4") || fileName.hasSuffix(".mov"))
                    {
                        try fileManager.removeItem(atPath: filePathName)
                        print("\(filePathName) eliminated")
                    }
                }
            }
            
            let files = try fileManager.contentsOfDirectory(atPath: "\(documentPath)")
            //print("all files in " + type + " after deleting videos: \(files)")
        }
        
    } catch {
        print("Could not clear \(type) folder: \(error)")
    }
}
