//
//  VideoEditor.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 05/01/21.
//

import Foundation

import UIKit
import AVFoundation

class VideoEditor {
    
    func renderVideo(
        fromVideoAt videoURL: URL,
        forName name: String,
        title_image: UIImage,
        gradient_colors: [UIColor],
        carousel_background_color:UIColor,
        background_asset: String,
        onComplete: @escaping (URL?
        ) -> Void) {

        let asset = AVURLAsset(url: videoURL)
        let composition = AVMutableComposition()
        
        guard
            let compositionTrack = composition.addMutableTrack(
                withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid),
            let assetTrack = asset.tracks(withMediaType: .video).first
        else {
            print("Something is wrong with the asset.")
            onComplete(nil)
            return
        }
        
        do {
            let timeRange = CMTimeRange(start: .zero, duration: asset.duration)
            try compositionTrack.insertTimeRange(timeRange, of: assetTrack, at: .zero)
            
            if let audioAssetTrack = asset.tracks(withMediaType: .audio).first,
               let compositionAudioTrack = composition.addMutableTrack(
                withMediaType: .audio,
                preferredTrackID: kCMPersistentTrackID_Invalid) {
                try compositionAudioTrack.insertTimeRange(
                    timeRange,
                    of: audioAssetTrack,
                    at: .zero)
            }
        } catch {
            print(error)
            onComplete(nil)
            return
        }
        
        compositionTrack.preferredTransform = assetTrack.preferredTransform
        let videoInfo = orientation(from: assetTrack.preferredTransform)
        
        let videoSize: CGSize
        if videoInfo.isPortrait {
            videoSize = CGSize(
                width: assetTrack.naturalSize.height,
                height: assetTrack.naturalSize.width)
        } else {
            videoSize = assetTrack.naturalSize
            //            print("video is landscape")
        }
        print("videoSize", videoSize)
        
        let osize = CGSize(width: 1080, height: 1920)
        

        
        let backgroundLayer = CALayer()
        backgroundLayer.frame = CGRect(origin: .zero, size: osize)
        let videoLayer = CALayer()
        videoLayer.frame = CGRect(origin: .zero, size: osize)
        let titleLayer = CALayer()
        titleLayer.frame = CGRect(origin: .zero, size: osize)
        let overlayLayer = CALayer()
        overlayLayer.frame = CGRect(origin: .zero, size: videoSize)
        
        let outputLayer = CALayer()
        outputLayer.frame = CGRect(origin: .zero, size: osize)
        
        print("local asset without asset:\(background_asset)")
        if ("file:///" != background_asset){
            print("ho trovato la stringa giusta")
            let backgroundAssetLayer = CALayer()
            backgroundAssetLayer.frame = CGRect(origin: .zero, size: osize)
            backgroundAssetLayer.contents = UIImage(contentsOfFile: background_asset)?.cgImage
            outputLayer.addSublayer(backgroundAssetLayer)
        } else {
            let gradient = CAGradientLayer()
            gradient.frame = CGRect(origin: .zero, size: osize)
            gradient.type = .axial
            gradient.colors = gradient_colors.map{ $0.cgColor }.reversed()
            gradient.locations = gradient_colors.count == 3 ? [0, 0.5, 1] : [0, 1]
            backgroundLayer.backgroundColor = carousel_background_color.cgColor
            outputLayer.addSublayer(gradient)
        }
        
//        let scaledVideoSize: CGSize = CGSize(width: videoSize.width * 0.65, height: videoSize.height * 0.65)
        let videoaspect = videoSize.width / videoSize.height
        let newVideoWidth = CGFloat(1000)
        let scaledVideoSize = CGSize(width: newVideoWidth, height: newVideoWidth * CGFloat(Int(videoSize.height)) / CGFloat(Int(videoSize.width)) )
        
        
        titleLayer.contents = title_image.cgImage
        print("title_image.size", title_image.size)
//        titleLayer.contentsGravity = .center
        let aspect: CGFloat = title_image.size.width / title_image.size.height
//        titleLayer.frame = CGRect(
//            x: osize.width / 2 - (videoSize.width / 2 ),
//            y: osize.height / 2 - (videoSize.height / 2) - 100,
//            width: videoSize.width,
//            height: videoSize.width / aspect)
        
        let newtitleheight = title_image.size.height*newVideoWidth/title_image.size.width
        titleLayer.frame = CGRect(
            x: osize.width / 2 - scaledVideoSize.width / 2,
            y: (osize.height / 2)  - (scaledVideoSize.height / 2) - newtitleheight,
            width: scaledVideoSize.width,
            height: newtitleheight
        )
        
//        addText(text: "titolo del video", to: videoLayer, videoSize: scaledVideoSize)
        
        videoLayer.frame = CGRect(
            x: osize.width / 2 - (scaledVideoSize.width / 2),
            y: osize.height * 0.5 - (scaledVideoSize.height / 2),
            width: scaledVideoSize.width ,
            height: scaledVideoSize.height)

        videoLayer.cornerRadius = 30.0
        videoLayer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        videoLayer.masksToBounds = true
        
//        videoLayer.frame = videoLayer.frame.insetBy(dx: 20, dy: 20)
//        videoLayer.borderWidth = 1.0
//        videoLayer.borderColor = UIColor.white.cgColor
        
//        videoLayer.shadowOpacity = 0.75
//        videoLayer.shadowOffset = CGSize(width: 10, height: 10)
//        videoLayer.shadowRadius = 10.0
              

//        outputLayer.addSublayer(backgroundLayer)
        outputLayer.addSublayer(videoLayer)
        outputLayer.addSublayer(titleLayer)
//        outputLayer.addSublayer(overlayLayer)
        
        let videoComposition = AVMutableVideoComposition()
        videoComposition.renderSize = osize
        videoComposition.frameDuration = CMTime(value: 1, timescale: 30)
        videoComposition.animationTool = AVVideoCompositionCoreAnimationTool(
            postProcessingAsVideoLayer: videoLayer,
            in: outputLayer)
        
        let instruction = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRange(
            start: .zero,
            duration: composition.duration)
        videoComposition.instructions = [instruction]
        let layerInstruction = compositionLayerInstruction(
            for: compositionTrack,
            assetTrack: assetTrack)
        instruction.layerInstructions = [layerInstruction]
        
        //        bug fixing strange video rendering size
        //        https://stackoverflow.com/questions/32211261/avfoundation-fit-video-to-calayer-correctly-when-exporting
        
        let bugFixTransform = CGAffineTransform(scaleX: osize.width/assetTrack.naturalSize.width,
                                                y: osize.height/assetTrack.naturalSize.height)
        layerInstruction.setTransform(bugFixTransform, at: .zero)
        
        guard let export = AVAssetExportSession(
                asset: composition,
                presetName: AVAssetExportPresetHighestQuality)
        else {
            print("Cannot create export session.")
            onComplete(nil)
            return
        }
        
        let videoName = UUID().uuidString
        let exportURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            .appendingPathComponent(videoName)
            .appendingPathExtension("mov")
        
        export.videoComposition = videoComposition
        export.outputFileType = .mov
        export.outputURL = exportURL
        
        export.exportAsynchronously {
            DispatchQueue.main.async {
                switch export.status {
                case .completed:
                    onComplete(exportURL)
                default:
                    print("Something went wrong during export.")
                    print(export.error ?? "unknown error")
                    onComplete(nil)
                    break
                }
            }
        }
    }
    
    func renderVideoBlur(
        video videoURL: URL,
        card_bottom: UIImage,
        ogvideosize: CGSize,
        onComplete: @escaping (URL?
        ) -> Void) {

        let asset = AVURLAsset(url: videoURL)
        let composition = AVMutableComposition()
        
        guard
            let compositionTrack = composition.addMutableTrack(
                withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid),
            let assetTrack = asset.tracks(withMediaType: .video).first
        else {
            print("Something is wrong with the asset.")
            onComplete(nil)
            return
        }
        
        do {
            let timeRange = CMTimeRange(start: .zero, duration: asset.duration)
            try compositionTrack.insertTimeRange(timeRange, of: assetTrack, at: .zero)
            
            if let audioAssetTrack = asset.tracks(withMediaType: .audio).first,
               let compositionAudioTrack = composition.addMutableTrack(
                withMediaType: .audio,
                preferredTrackID: kCMPersistentTrackID_Invalid) {
                try compositionAudioTrack.insertTimeRange(
                    timeRange,
                    of: audioAssetTrack,
                    at: .zero)
            }
        } catch {
            print(error)
            onComplete(nil)
            return
        }
        
        compositionTrack.preferredTransform = assetTrack.preferredTransform
        let videoInfo = orientation(from: assetTrack.preferredTransform)
        
        let videoSize: CGSize
        if videoInfo.isPortrait {
            videoSize = CGSize(
                width: assetTrack.naturalSize.height,
                height: assetTrack.naturalSize.width)
        } else {
            videoSize = assetTrack.naturalSize
            //            print("video is landscape")
        }
        print("videoSize", videoSize)
        
        let osize = CGSize(width: 1080, height: 1920)
        

        
//        let backgroundLayer = CALayer()
//        backgroundLayer.frame = CGRect(origin: .zero, size: osize)
        let videoLayer = CALayer()
        videoLayer.frame = CGRect(origin: .zero, size: osize)
        let titleLayer = CALayer()
        titleLayer.frame = CGRect(origin: .zero, size: osize)
//        let overlayLayer = CALayer()
//        overlayLayer.frame = CGRect(origin: .zero, size: osize)
        
        let outputLayer = CALayer()
        outputLayer.frame = CGRect(origin: .zero, size: osize)
        
        
//        let scaledVideoSize: CGSize = CGSize(width: videoSize.width * 0.65, height: videoSize.height * 0.65)
//        let videoaspect = videoSize.width / videoSize.height
        let newVideoWidth = CGFloat(1000)
        let scaledVideoSize = CGSize(width: newVideoWidth, height: newVideoWidth * CGFloat(Int(videoSize.height)) / CGFloat(Int(videoSize.width)) )
//
        
        titleLayer.contents = card_bottom.cgImage
        print("title_image.size", card_bottom.size)
        let aspect: CGFloat = card_bottom.size.width / card_bottom.size.height

        
//        titleLayer.frame = CGRect(
//            x: osize.width / 2 - scaledVideoSize.width / 2,
//            y: (osize.height * 0.5)  - (scaledVideoSize.height / 2) - (scaledVideoSize.width / aspect),
//            width: scaledVideoSize.width,
//            height: scaledVideoSize.width / aspect)
        
        // probably x0 y0 is the bottom left corner
        print("ogvideosize", ogvideosize)
        
        titleLayer.frame = CGRect(
            x: osize.width / 2 - scaledVideoSize.width / 2,
//            y: (osize.height / 2) - ogvideosize.height - card_bottom.size.height - 20,
            y: osize.height * 0.25,
            width: scaledVideoSize.width,
            height: scaledVideoSize.width / aspect)
        
        
        outputLayer.addSublayer(videoLayer)
        outputLayer.addSublayer(titleLayer)
        
        let videoComposition = AVMutableVideoComposition()
        videoComposition.renderSize = osize
        videoComposition.frameDuration = CMTime(value: 1, timescale: 30)
        videoComposition.animationTool = AVVideoCompositionCoreAnimationTool(
            postProcessingAsVideoLayer: videoLayer,
            in: outputLayer)
        
        let instruction = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRange(
            start: .zero,
            duration: composition.duration)
        videoComposition.instructions = [instruction]
        let layerInstruction = compositionLayerInstruction(
            for: compositionTrack,
            assetTrack: assetTrack)
        instruction.layerInstructions = [layerInstruction]
        
        //        bug fixing strange video rendering size
        //        https://stackoverflow.com/questions/32211261/avfoundation-fit-video-to-calayer-correctly-when-exporting
        
        let bugFixTransform = CGAffineTransform(scaleX: osize.width/assetTrack.naturalSize.width,
                                                y: osize.height/assetTrack.naturalSize.height)
        layerInstruction.setTransform(bugFixTransform, at: .zero)
        
        guard let export = AVAssetExportSession(
                asset: composition,
                presetName: AVAssetExportPresetHighestQuality)
        else {
            print("Cannot create export session.")
            onComplete(nil)
            return
        }
        
        let videoName = UUID().uuidString
        let exportURL = URL(fileURLWithPath: NSTemporaryDirectory())
            .appendingPathComponent(videoName)
            .appendingPathExtension("mov")
        
        export.videoComposition = videoComposition
        export.outputFileType = .mov
        export.outputURL = exportURL
        
        export.exportAsynchronously {
            DispatchQueue.main.async {
                switch export.status {
                case .completed:
                    onComplete(exportURL)
                default:
                    print("Something went wrong during export.")
                    print(export.error ?? "unknown error")
                    onComplete(nil)
                    break
                }
            }
        }
    }
    
    private func addImage(to layer: CALayer, videoSize: CGSize) {
        let image = UIImage(named: "overlay")!
        let imageLayer = CALayer()
        
        let aspect: CGFloat = image.size.width / image.size.height
        let width = videoSize.width
        let height = width / aspect
        imageLayer.frame = CGRect(
            x: 0,
            y: -height * 0.15,
            width: width,
            height: height)
        
        imageLayer.contents = image.cgImage
        layer.addSublayer(imageLayer)
    }
    
    private func addText(text: String, to layer: CALayer, videoSize: CGSize) {
        let attributedText = NSAttributedString(
            string: text,
            attributes: [
                .font: UIFont.systemFont(ofSize: 30),
                .foregroundColor: UIColor.black.cgColor])
        
        let textLayer = CATextLayer()
        textLayer.string = attributedText
        textLayer.shouldRasterize = true
        textLayer.rasterizationScale = UIScreen.main.scale
        textLayer.backgroundColor = UIColor.white.cgColor
        textLayer.alignmentMode = .left
        
        textLayer.frame = CGRect(
            x: 0,
            y: -100,
            width: videoSize.width,
            height: 100)
        textLayer.cornerRadius = 10
        
        textLayer.displayIfNeeded()
        
        //        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        //        scaleAnimation.fromValue = 0.8
        //        scaleAnimation.toValue = 1.2
        //        scaleAnimation.duration = 0.5
        //        scaleAnimation.repeatCount = .greatestFiniteMagnitude
        //        scaleAnimation.autoreverses = true
        //        scaleAnimation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        //
        //        scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero
        //        scaleAnimation.isRemovedOnCompletion = false
        //        textLayer.add(scaleAnimation, forKey: "scale")
        
        layer.addSublayer(textLayer)
    }
    
    private func add(text: String, to layer: CALayer, videoSize: CGSize) {
        let attributedText = NSAttributedString(
            string: text,
            attributes: [
                .font: UIFont(name: "ArialRoundedMTBold", size: 60) as Any,
                .foregroundColor: UIColor.black.cgColor,
                .strokeColor: UIColor.white,
                .strokeWidth: -3])
        
        let textLayer = CATextLayer()
        textLayer.string = attributedText
        textLayer.shouldRasterize = true
        textLayer.rasterizationScale = UIScreen.main.scale
        textLayer.backgroundColor = UIColor.white.cgColor
        textLayer.alignmentMode = .left
        
        textLayer.frame = CGRect(
            x: 0,
            y: -100,
            width: videoSize.width,
            height: 100)
        textLayer.displayIfNeeded()
        
        //        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        //        scaleAnimation.fromValue = 0.8
        //        scaleAnimation.toValue = 1.2
        //        scaleAnimation.duration = 0.5
        //        scaleAnimation.repeatCount = .greatestFiniteMagnitude
        //        scaleAnimation.autoreverses = true
        //        scaleAnimation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        //
        //        scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero
        //        scaleAnimation.isRemovedOnCompletion = false
        //        textLayer.add(scaleAnimation, forKey: "scale")
        
        layer.addSublayer(textLayer)
    }
    
    private func orientation(from transform: CGAffineTransform) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
        var assetOrientation = UIImage.Orientation.up
        var isPortrait = false
        if transform.a == 0 && transform.b == 1.0 && transform.c == -1.0 && transform.d == 0 {
            assetOrientation = .right
            isPortrait = true
        } else if transform.a == 0 && transform.b == -1.0 && transform.c == 1.0 && transform.d == 0 {
            assetOrientation = .left
            isPortrait = true
        } else if transform.a == 1.0 && transform.b == 0 && transform.c == 0 && transform.d == 1.0 {
            assetOrientation = .up
        } else if transform.a == -1.0 && transform.b == 0 && transform.c == 0 && transform.d == -1.0 {
            assetOrientation = .down
        }
        
        return (assetOrientation, isPortrait)
    }
    
    private func compositionLayerInstruction(for track: AVCompositionTrack, assetTrack: AVAssetTrack) -> AVMutableVideoCompositionLayerInstruction {
        let instruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track)
        let transform = assetTrack.preferredTransform
        
        instruction.setTransform(transform, at: .zero)
        
        return instruction
    }
    
    private func addConfetti(to layer: CALayer) {
        let images: [UIImage] = (0...5).map { UIImage(named: "confetti\($0)")! }
        let colors: [UIColor] = [.systemGreen, .systemRed, .systemBlue, .systemPink, .systemOrange, .systemPurple, .systemYellow]
        let cells: [CAEmitterCell] = (0...16).map { _ in
            let cell = CAEmitterCell()
            cell.contents = images.randomElement()?.cgImage
            cell.birthRate = 3
            cell.lifetime = 12
            cell.lifetimeRange = 0
            cell.velocity = CGFloat.random(in: 100...200)
            cell.velocityRange = 0
            cell.emissionLongitude = 0
            cell.emissionRange = 0.8
            cell.spin = 4
            cell.color = colors.randomElement()?.cgColor
            cell.scale = CGFloat.random(in: 0.2...0.8)
            return cell
        }
        
        let emitter = CAEmitterLayer()
        emitter.emitterPosition = CGPoint(x: layer.frame.size.width / 2, y: layer.frame.size.height + 5)
        emitter.emitterShape = .line
        emitter.emitterSize = CGSize(width: layer.frame.size.width, height: 2)
        emitter.emitterCells = cells
        
        layer.addSublayer(emitter)
    }
}
