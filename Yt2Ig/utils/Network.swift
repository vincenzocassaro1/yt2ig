//
//  Network.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 02/03/22.
//

import Foundation

// MARK: - Fetching with retry

func fetchWithRetry(url: String, completion: @escaping (_ exist: Data?, _ error: String?) -> Void) {
    
    var request = URLRequest(url: URL(string: url)!)
    request.httpMethod = "GET"
    
    requestWithRetry(with: request) {
        (data, response, error, retriesLeft) in
        
        if let error = error {
            completion(nil, "\(error.localizedDescription) with \(retriesLeft) retries left")
            return
        }
        
        let statusCode = (response as! HTTPURLResponse).statusCode
        
        if statusCode == 200, let data = data {
            let exist = true
            completion(data, nil)
        } else {
            completion(nil, "Error encountered: \(statusCode) with \(retriesLeft) retries left")
        }
    }
}

let session = URLSession(
    configuration: URLSessionConfiguration.default,
    delegate: nil,
    delegateQueue: nil
)

// **** This function is recursive, and will automatically retry
private func requestWithRetry(with request: URLRequest, retries: Int = 5, completionHandler: @escaping (Data?, URLResponse?, Error?, _ retriesLeft: Int) -> Void) {
    let task = session.dataTask(with: request) {
        (data, response, error) in
        if error != nil {
            completionHandler(data, response, error, retries)
            return
        }
        
        let statusCode = (response as! HTTPURLResponse).statusCode
        
        if (statusCode == 200) {
            completionHandler(data, response, error, retries)
        }
        else
        {
            var seconds = 2.0
            //                if retries == 5 {
            //                    seconds = 0
            //                }
            
            print(retries, seconds)
            DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                // Put your code which should be executed with a delay here
                // print("Received status code \(statusCode) with \(retries) retries remaining. RETRYING VIA RECURSIVE CALL.")
                requestWithRetry(with: request,
                                 retries: retries,
                                 completionHandler: completionHandler)
            }
            
        }
        //            else {
        //                print("Received status code \(statusCode) with \(retries) retries remaining. EXIT WITH FAILURE.")
        //                completionHandler(data, response, error, retries)
        //            }
    }
    task.resume()
}

func postRequest(urlString: String, completion: @escaping (Result<Video, NetworkError>) -> Void) {
    // check the URL is OK, otherwise return with a failure
    guard let encoded = try? JSONEncoder().encode(VideoRequest(url: urlString)) else {
        //print("Failed to encode order")
        completion(.failure(.badURL))
        return
    }
    
      var __baseurl = "http://3.68.124.191:5000/v2"
//    var __baseurl = "http://192.168.1.48:5000/v2"
    guard let server_url = URL(string: __baseurl) else {
        print("Invalid URL")
        completion(.failure(.badURL))
        return
    }
    
    // create post request
    var request = URLRequest(url: server_url)
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    request.httpMethod = "POST"
    request.httpBody = encoded
    
    URLSession.shared.dataTask(with: request) { data, response, error in
        // the task has completed – push our work back to the main thread
        
        if error != nil {
            // OH NO! An error occurred...
            //                self.handleClientError(error)
            return
        }
        
        guard let httpResponse = response as? HTTPURLResponse,
              (200...299).contains(httpResponse.statusCode) else {
            //                self.handleServerError(response)
            //print(response)
            //print("server error")
            completion(.failure(.requestFailed))
            return
        }
        
        DispatchQueue.main.async {
            if let data = data {
                // success: convert the data to a string and send it back
                let decodedResponse = try! JSONDecoder().decode(Video.self, from: data)
                completion(.success(decodedResponse))
            } else if error != nil {
                // any sort of network failure
                //print(error)
                completion(.failure(.requestFailed))
            } else {
                // this ought not to be possible, yet here we are
                completion(.failure(.unknown))
            }
        }
    }.resume()
}

func loadFileAsync(url: URL, completion: @escaping (String?, Error?) -> ()) {
    
    let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    
    let destinationUrl = documentsUrl.appendingPathComponent(url.lastPathComponent)
    
    if FileManager().fileExists(atPath: destinationUrl.path)
    {
        print("File already exists [\(destinationUrl.path)]")
        completion(destinationUrl.path, nil)
    }
    else
    {
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request, completionHandler:
                                        {
                                            data, response, error in
                                            if error == nil
                                            {
                                                if let response = response as? HTTPURLResponse
                                                {
                                                    if response.statusCode == 200
                                                    {
                                                        if let data = data
                                                        {
                                                            if let _ = try? data.write(to: destinationUrl, options: Data.WritingOptions.atomic)
                                                            {
                                                                completion(destinationUrl.path, error)
                                                            }
                                                            else
                                                            {
                                                                completion(destinationUrl.path, error)
                                                            }
                                                        }
                                                        else
                                                        {
                                                            completion(destinationUrl.path, error)
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                completion(destinationUrl.path, error)
                                            }
                                        })
        task.resume()
    }
}
