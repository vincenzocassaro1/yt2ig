//
//  YouTube.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 03/02/23.
//

import UIKit

func openYoutube() -> Void{
    let urlScheme = URL(string: "youtube://")!
    let sharedApp = UIApplication.shared
    let youtubeWebsite = URL(string: "https://youtube.com")!
    
    if sharedApp.canOpenURL(urlScheme) {
        sharedApp.open(urlScheme, options: [:], completionHandler: nil)
    } else {
        print("Cannot open \(urlScheme), opening website")
        sharedApp.open(youtubeWebsite)
    }
}
