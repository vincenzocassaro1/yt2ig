//
//  Yt2IgApp.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 04/12/20.
//

import SwiftUI

@main
struct Yt2IgApp: App {
    var downloadManager = DownloadManager()

    var body: some Scene {
        WindowGroup {
            
            FirstView()
        }
    }
    
}
