//
//  Video.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 07/03/22.
//

import Foundation

/// A type that has an "empty" representation.
public protocol EmptyRepresentable {
  static func empty() -> Self
}

extension Array: EmptyRepresentable {
  public static func empty() -> Array<Element> {
    return Array()
  }
}

extension KeyedDecodingContainer {
  public func decode<T>(_ type: T.Type, forKey key: KeyedDecodingContainer<K>.Key) throws -> T where T: Decodable & EmptyRepresentable {
    if let result = try decodeIfPresent(T.self, forKey: key) {
      return result
    } else {
      return T.empty()
    }
  }
}

struct Video: Codable {
    var thumbnail: String
    var title: String
    var uploader: String
    var trimmed_video_url: String
    var video_width: String
    var video_height: String
    var duration: String
    var view_count: String
    var channel_assets: [String]
}
