//
//  VideoRequest.swift
//  Yt2Ig
//
//  Created by Vincenzo Cassaro on 07/03/22.
//

import Foundation

struct VideoRequest: Codable {
    var url: String
}
